/**
 * Created by Jessie on 16/11/16.
 */

'use strict';

var report = require('./report.html');
var district = require('./district.html');
var roadUsageRqi = require('./roadUsageRqi.html');
var roadUsageDef = require('./roadUsageDef.html');
var roadUsageAadt = require('./roadUsageAadt.html');
var roadAdviceMaintain = require('./roadAdviceMaintain.html');
var roadAdviceMaintain2 = require('./roadAdviceMaintain2.html');
var roadAdviceList = require('./roadAdviceList.html');
var roadPlanPerformance = require('./roadPlanPerformance.html');
var roadPlanPerformance2 = require('./roadPlanPerformance2.html');
var compareDistrict = require('./compare-district.html');
var sidebar = require('./sidebar.html');
var sidebarDistrict = require('./sidebarDistrict.html');
//地区
var districtRoadUsageRqi = require('./districtRoadUsageRqi.html');
var districtRoadUsageDef = require('./districtRoadUsageDef.html');
var districtRoadUsageAadt = require('./districtRoadUsageAadt.html');
var districtRoadAdviceMaintain = require('./districtRoadAdviceMaintain.html');
var districtRoadAdviceMaintain2 = require('./districtRoadAdviceMaintain2.html');
var districtRroadAdviceList = require('./districtRoadAdviceList.html');
var districtRoadPlanPerformance = require('./districtRoadPlanPerformance.html');
var districtRoadPlanPerformance2 = require('./districtRoadPlanPerformance2.html');
var districtRoadPlanPerformance3 = require('./districtRoadPlanPerformance3.html');
var dynamicList = require('./dynamicList.html');
var districtSummaryDetail = require('./districtSummaryDetail.html');
var districtSummaryList = require('./districtSummaryList.html');
angular
  .module('app.report', [])
  .config(['$routeProvider', '$locationProvider', '$httpProvider', function ($routeProvider, $locationProvider, $httpProvider) {
    $routeProvider
      .when('/statistics/report', {
        templateUrl: report,
        controller: 'reportController',
        state: 'report',
        moduleName: 'report',
        tableName: 'report'
      })
      .when('/statistics/road-usage/rqi', {
        templateUrl: roadUsageRqi,
        controller: 'reportRqiController',
        state: 'report', // 大等级
        moduleName: 'road-usage', // 链接二级
        tableName: 'rqi' // 链接三级
      })
      .when('/statistics/road-usage/pci', {
        templateUrl: roadUsageRqi,
        controller: 'reportRqiController',
        state: 'report',
        moduleName: 'road-usage',
        tableName: 'pci'
      })
      .when('/statistics/road-usage/def', {
        templateUrl: roadUsageDef,
        controller: 'reportDefController',
        state: 'report',
        moduleName: 'road-usage',
        tableName: 'def'
      })
      .when('/statistics/road-usage/aadt', {
        templateUrl: roadUsageAadt,
        controller: 'reportAadtController',
        state: 'report',
        moduleName: 'road-usage',
        tableName: 'aadt'
      })
      .when('/statistics/road-advice/maintain', {
        templateUrl: roadAdviceMaintain,
        controller: 'roadAdviceMaintainController',
        state: 'report',
        moduleName: 'road-advice',
        tableName: 'maintain'
      })
      .when('/statistics/road-advice/maintain2', {
        templateUrl: roadAdviceMaintain2,
        controller: 'roadAdviceMaintain2Controller',
        state: 'report',
        moduleName: 'road-advice',
        tableName: 'maintain2'
      })
      .when('/statistics/maintain', {
        templateUrl: roadAdviceList,
        controller: 'roadAdviceListController',
        state: 'report',
        moduleName: 'maintain'
      })
      .when('/statistics/road-plan/performance', {
        templateUrl: roadPlanPerformance,
        controller: 'roadPlanPerformanceController',
        state: 'report',
        moduleName: 'road-plan',
        tableName: 'performance'
      })
      .when('/statistics/road-plan/performance2', {
        templateUrl: roadPlanPerformance2,
        controller: 'roadPlanPerformance2Controller',
        state: 'report',
        moduleName: 'road-plan',
        tableName: 'performance2'
      })
      .when('/statistics/compare-district/:district', {
        templateUrl: compareDistrict,
        controller: 'compareDistrictController',
        state: 'report',
        moduleName: 'compare-district',
        tableName: ''
      })
      // 分区报表
      .when('/statistics', {
        templateUrl: district,
        controller: 'districtController',
        state: 'statistics',
        moduleName: 'district',
        tableName: 'district'
      })
      .when('/statistics/district/:district', {
        templateUrl: district,
        controller: 'districtController',
        state: 'district',
        moduleName: 'district',
        tableName: 'district'
      })
      .when('/statistics/road-usage/rqi/:district', {
        templateUrl: districtRoadUsageRqi,
        controller: 'districtRqiController',
        state: 'district',
        moduleName: 'road-usage', // 链接二级
        tableName: 'rqi' // 链接三级
      })
      .when('/statistics/road-usage/pci/:district', {
        templateUrl: districtRoadUsageRqi,
        controller: 'districtRqiController',
        state: 'district',
        moduleName: 'road-usage', // 链接二级
        tableName: 'pci' // 链接三级
      })
      .when('/statistics/road-usage/def/:district', {
        templateUrl: districtRoadUsageDef,
        controller: 'districtDefController',
        state: 'district',
        moduleName: 'road-usage', // 链接二级
        tableName: 'def' // 链接三级
      })
      .when('/statistics/road-usage/aadt/:district', {
        templateUrl: districtRoadUsageAadt,
        controller: 'districtAadtController',
        state: 'district',
        moduleName: 'road-usage', // 链接二级
        tableName: 'aadt' // 链接三级
      })
      .when('/statistics/road-advice/maintain/:district', {
        templateUrl: districtRoadAdviceMaintain,
        controller: 'districtRoadAdviceMaintainController',
        state: 'district',
        moduleName: 'road-advice',
        tableName: 'maintain'
      })
      .when('/statistics/road-advice/maintain2/:district', {
        templateUrl: districtRoadAdviceMaintain2,
        controller: 'districtRoadAdviceMaintain2Controller',
        state: 'district',
        moduleName: 'road-advice',
        tableName: 'maintain2'
      })
      .when('/statistics/maintain/:district', {
        templateUrl: districtRroadAdviceList,
        controller: 'districtRroadAdviceListController',
        state: 'district',
        moduleName: 'maintain'
      })
      .when('/statistics/road-plan/performance/:district', {
        templateUrl: districtRoadPlanPerformance,
        controller: 'districtRoadPlanPerformanceController',
        state: 'district',
        moduleName: 'road-plan',
        tableName: 'performance'
      })
      .when('/statistics/road-plan/performance2/:district', {
        templateUrl: districtRoadPlanPerformance2,
        controller: 'districtRoadPlanPerformance2Controller',
        state: 'district',
        moduleName: 'road-plan',
        tableName: 'performance2'
      })
      .when('/statistics/road-plan/performance3/:district', {
        templateUrl: districtRoadPlanPerformance3,
        controller: 'districtRoadPlanPerformance3Controller',
        state: 'district',
        moduleName: 'road-plan',
        tableName: 'performance3'
      })
      .when('/statistics/dynamic/:district', {
        templateUrl: dynamicList,
        controller: 'dynamicListController',
        state: 'district',
        moduleName: 'dynamic'
      })
      .when('/statistics/summaryList/:district', {
        templateUrl: districtSummaryList,
        controller: 'districtSummaryListController',
        state: 'district',
        moduleName: 'summaryList'
      })
      .when('/statistics/summary/:district/:sid', {
        templateUrl: districtSummaryDetail,
        controller: 'districtSummaryDetailController',
        state: 'district',
        moduleName: 'summary'
      })
      ;
  }])
  .controller('reportController', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', '$route', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope, $route) {
    $scope.currPage = 0;
    var gradeList = $scope.gradeList = {};

    var formGrade = $scope.formGrade = {
      year: $rootScope.globalYear
    };

    var infoParams = {
      year: $rootScope.globalYear,
    }

    var formDistrict = $scope.formDistrict = {
      year: $rootScope.globalYear
    };

    function getDataByGrade() {
      reportIF.getSummaryDataByGrade(formGrade).success(function (data) {
        if (data.status == 0) {
          $scope.gradeList = data.data || [];
        }
      });
    }
    function getDataByDistrict() {
      reportIF.getSummaryDataByDistrict(formDistrict).success(function (data) {
        if (data.status == 0) {
          $scope.districtList = data.data || [];
        }
      });
    }

    $scope.changeNav = function (index) {
      if ($scope.currPage === index) {
        return;
      }
      $scope.currPage = index;
    };

    function pageInfo() {
      reportIF.summaryPageinfo(infoParams).success(function (data) {
        if (data.status == 0) {
          $scope.pageInfo = data.data || {};
        }
      });
    }


    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '暂时没有该年份的数据，请导入' + $rootScope.globalYear + '年份的动静态数据';

      formGrade.year = data;
      infoParams.year = data;
      formDistrict.year = data;

      getDataByGrade();
      getDataByDistrict();
      pageInfo();
    });
    $rootScope.globalWatch.watchReport = watch;
    $scope.$on("$destroy", function (ev, to, to2) {
      watch();
    })
  }])
  .controller('reportRqiController', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', '$route', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope, $route) {
    var getTableName = $scope.getTableName = $route.current.tableName; // 有相似模块
    var moduleName = $route.current.moduleName;
    var pageData = {};
    if (getTableName == 'rqi') {
      $scope.newTableName = 'RQI';
    } else if (getTableName == 'pci') {
      $scope.newTableName = 'PCI';
    }
    var gradeList = $scope.gradeList = {};
    var formGrade = $scope.formGrade = {
      year: $rootScope.globalYear
    };

    var formDistrict = $scope.formDistrict = {
      year: $rootScope.globalYear
    };

    // 检测道路合计RQI分区比较图          
    function initDistrictChart2(data) {
      var xAxis = [];
      var chartData = [];
      angular.forEach(data, function (v, k) {
        if (v.name == '总计') {
          return;
        }
        angular.forEach(v.list, function (value, key) {
          if (value.district == '总计') {
            return;
          }
          xAxis.push(value.district);
          if (getTableName == 'rqi') {
            chartData.push(Number(value.avg_rqi).toFixed(2));
          } else if (getTableName == 'pci') {
            chartData.push(Number(value.avg_pci).toFixed(2));
          }
        });
      });

      $scope.districtChartData2 = {
        xAxis: [{
          type: 'category',
          data: xAxis
        }],
        yAxis: [{
          type: 'value',
          name: getTableName == 'rqi' ? 'RQI' : 'PCI'
        }],
        series: [{
          type: 'bar',
          data: chartData,
          itemStyle: {
            normal: {
              label: {
                show: true,
                position: 'top'
              }
            }
          }
        }]
      };
    }

    function accAdd(arg1, arg2) {
      var r1, r2, m;
      try { r1 = arg1.toString().split(".")[1].length } catch (e) { r1 = 0 }
      try { r2 = arg2.toString().split(".")[1].length } catch (e) { r2 = 0 }
      m = Math.pow(10, Math.max(r1, r2))
      return (arg1 * m + arg2 * m) / m
    }

    function accSub(arg1, arg2) {
      return accAdd(arg1, -arg2);
    }

    // 检测道路合计RQI分区统计图
    function initDistrictChart(data) {
      var xAxis = [];
      var chartData = [{
        name: '优级',
        type: 'bar',
        data: [],
        stack: 'group'
      }, {
        name: '良好',
        type: 'bar',
        data: [],
        stack: 'group'
      }, {
        name: '合格',
        type: 'bar',
        data: [],
        stack: 'group'
      }, {
        name: '不合格',
        type: 'bar',
        data: [],
        stack: 'group'
      }];
      var legend = ['优级', '良好', '合格', '不合格'];
      angular.forEach(data, function (v, k) {
        if (v.name == '总计') {
          return;
        }
        angular.forEach(v.list, function (value, key) {
          if (value.district == '总计') {
            return;
          }
          xAxis.push(value.district);
          chartData[0].data.push(value.youxiu_area_p);
          chartData[1].data.push(value.lianghao_area_p);
          chartData[2].data.push(value.jige_area_p);
          if (accAdd(accAdd(accAdd(value.youxiu_area_p, value.lianghao_area_p), value.jige_area_p), value.bujige_area_p) > 100) {
            chartData[3].data.push(accSub(99.99, accAdd(accAdd(value.youxiu_area_p, value.lianghao_area_p), value.jige_area_p)));
          } else {
            chartData[3].data.push(value.bujige_area_p);
          }
        });
      });
      $scope.districtChartData = {
        xAxis: [{
          type: 'category',
          data: xAxis
        }],
        legend: {
          data: legend
        },
        yAxis: [{
          type: 'value',
          name: '长度百分比(%)'
        }],
        series: chartData
      };
      console.log($scope.districtChartData);
    }

    // 检测道路合计RQI分级比例图
    function initDistrictChart3(data) {
      var districtPieData = [];
      var legend = ['优级', '良好', '合格', '不合格'];
      angular.forEach(data, function (v, k) {
        if (v.name == '总计') {
          return;
        }
        var chartData = [{
          name: '优级',
          value: v.list[v.list.length - 1].youxiu_area_p
        }, {
          name: '良好',
          value: v.list[v.list.length - 1].lianghao_area_p
        }, {
          name: '合格',
          value: v.list[v.list.length - 1].jige_area_p
        }, {
          name: '不合格',
          value: v.list[v.list.length - 1].bujige_area_p
        }];
        var params = {
          legend: {
            data: legend
          },
          district: v.name,
          series: [
            {
              name: 'RQI',
              type: 'pie',
              radius: '55%',
              center: ['50%', '60%'],
              data: chartData,
              itemStyle: {
                normal: {
                  label: {
                    position: 'top',
                    formatter: function (params) {
                      return params.name + '，' + (params.percent - 0).toFixed(2) + '%'
                    }
                  }
                },
              },
            }
          ]
        }
        districtPieData.push(params);
      });
      $scope.districtPieData = districtPieData;
    }

    // 检测道路分等级RQI分区比较图
    function initGradeChart(data) {
      var xAxis = [];
      var chartData = [];
      angular.forEach(data, function (v, k) {
        if (v.name == '总计') {
          return;
        }
        angular.forEach(v.list, function (value, key) {
          if (value.district == '总计') {
            return;
          }
          xAxis.push(value.grade);
          if (getTableName == 'rqi') {
            chartData.push(value.avg_rqi);
          } else if (getTableName == 'pci') {
            chartData.push(value.avg_pci);
          }
        });
      });

      $scope.gradeChartData = {
        xAxis: [{
          type: 'category',
          data: xAxis
        }],
        yAxis: [{
          type: 'value',
          name: getTableName == 'rqi' ? 'RQI' : 'PCI'
        }],
        series: [{
          type: 'bar',
          data: chartData,
          itemStyle: {
            normal: {
              label: {
                show: true,
                position: 'top'
              }
            }
          }
        }]
      };
    }

    function getDataByGrade() {
      reportIF.getDataByGrade(formGrade, moduleName, getTableName).success(function (data) {
        if (data.status == 0) {
          $scope.gradeList = data.data || [];
          initGradeChart(data.data || [])
        }
      });
    }
    function getDataByDistrict() {
      reportIF.getDataByDistrict(formDistrict, moduleName, getTableName).success(function (data) {
        if (data.status == 0) {
          $scope.districtList = data.data || [];
          initDistrictChart(data.data || []);
          initDistrictChart2(data.data || []);
          initDistrictChart3(data.data || []);
        }
      });
    }

    function pageInfo() {
      reportIF.pageInfo().success(function (data) {
        if (data.status == 0) {
          pageData = data.data;
          getDataByGrade();
          getDataByDistrict();
        }
      });
    }
    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '需首先在「数据导入」中完成「数据库」的动态数据导入，并在「分析评价」中完成「数据库」的PQI、RQI、ESAL的计算后方可查看';

      formGrade.year = data;
      formDistrict.year = data;

      pageInfo();
    });
    $rootScope.globalWatch.watchReportRqi = watch;
    $scope.$on("$destroy", function () {
      watch();
    });
  }])
  .controller('reportDefController', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', '$route', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope, $route) {
    var getTableName = $scope.getTableName = $route.current.tableName; // 有相似模块
    var moduleName = $route.current.moduleName;
    $scope.newTableName = 'DEF';
    var gradeList = $scope.gradeList = {};
    var formGrade = $scope.formGrade = {
      year: $rootScope.globalYear
    };

    var formDistrict = $scope.formDistrict = {
      year: $rootScope.globalYear
    };

    // 检测道路合计DEF分区比较图         
    function initDistrictChart2(data) {
      var xAxis = [];
      var chartData = [];
      angular.forEach(data, function (v, k) {
        if (v.name == '总计') {
          return;
        }
        angular.forEach(v.list, function (value, key) {
          if (value.district == '总计') {
            return;
          }
          xAxis.push(value.district);
          chartData.push(value.linjie);
        });
      });

      $scope.districtChartData2 = {
        xAxis: [{
          type: 'category',
          data: xAxis
        }],
        yAxis: [{
          type: 'value',
          name: '弯沉在临界及以上所占比例（%）'
        }],
        series: [{
          type: 'bar',
          data: chartData,
          itemStyle: {
            normal: {
              label: {
                show: true,
                position: 'top'
              }
            }
          }
        }]
      };
    }

    // 检测道路合计DEF分区统计图
    function initDistrictChart(data) {
      var xAxis = [];
      var chartData = [{
        name: '足够',
        type: 'bar',
        data: [],
        stack: 'group'
      }, {
        name: '临界',
        type: 'bar',
        data: [],
        stack: 'group'
      }, {
        name: '不足',
        type: 'bar',
        data: [],
        stack: 'group'
      }];
      var legend = ['足够', '临界', '不足'];
      angular.forEach(data, function (v, k) {
        if (v.name == '总计') {
          return;
        }
        angular.forEach(v.list, function (value, key) {
          if (value.district == '总计') {
            return;
          }
          xAxis.push(value.district);
          chartData[0].data.push(value.zugou_area_p);
          chartData[1].data.push(value.linjie_area_p);
          chartData[2].data.push(value.buzu_area_p);
        });
      });
      $scope.districtChartData = {
        xAxis: [{
          type: 'category',
          data: xAxis
        }],
        legend: {
          data: legend
        },
        yAxis: [{
          type: 'value',
          name: '各区面积百分比（%）'
        }],
        series: chartData
      };
    }

    // 检测道路合计DEF分级比例图
    function initDistrictChart3(data) {
      var districtPieData = [];
      var legend = ['足够', '临界', '不足'];
      angular.forEach(data, function (v, k) {
        if (k == data.length - 1) {
          return;
        }
        var chartData = [{
          name: '足够',
          value: v.list[v.list.length - 1].zugou_area_p
        }, {
          name: '临界',
          value: v.list[v.list.length - 1].linjie_area_p
        }, {
          name: '不足',
          value: v.list[v.list.length - 1].buzu_area_p
        }];
        var params = {
          legend: {
            data: legend
          },
          district: v.name,
          series: [
            {
              name: v.name,
              type: 'pie',
              radius: '55%',
              center: ['50%', '60%'],
              data: chartData,
              itemStyle: {
                normal: {
                  label: {
                    position: 'top',
                    formatter: function (params) {
                      return params.name + '，' + (params.percent - 0).toFixed(2) + '%'
                    }
                  }
                },
              }
            }
          ]
        }
        districtPieData.push(params);
      });
      $scope.districtPieData = districtPieData;
    }

    // 检测道路分等级DEF分区比较图
    function initGradeChart(data) {
      var xAxis = [];
      var chartData = [];
      angular.forEach(data, function (v, k) {
        if (v.name == '总计') {
          return;
        }
        angular.forEach(v.list, function (value, key) {
          if (value.district == '总计') {
            return;
          }
          xAxis.push(v.name + value.grade);
          chartData.push(value.linjie);
        });
      });

      $scope.gradeChartData = {
        xAxis: [{
          type: 'category',
          data: xAxis
        }],
        yAxis: [{
          type: 'value',
          name: '弯沉在临界及以上所占比例（%）'
        }],
        series: [{
          type: 'bar',
          data: chartData,
          itemStyle: {
            normal: {
              label: {
                show: true,
                position: 'top'
              }
            }
          }
        }]
      };
    }

    function getDataByGrade() {
      reportIF.getDataByGrade(formGrade, moduleName, getTableName).success(function (data) {
        if (data.status == 0) {
          $scope.gradeList = data.data || [];
          initDistrictChart3(data.data || []);
          initGradeChart(data.data || [])
        }
      });
    }
    function getDataByDistrict() {
      reportIF.getDataByDistrict(formDistrict, moduleName, getTableName).success(function (data) {
        if (data.status == 0) {
          $scope.districtList = data.data || [];
          initDistrictChart3(data.data || []);
          initDistrictChart(data.data || []);
          initDistrictChart2(data.data || []);
        }
      });
    }

    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '需首先在「数据导入」中完成「数据库」的动态数据导入，并在「分析评价」中完成「数据库」的PQI、RQI、ESAL的计算后方可查看';

      formGrade.year = data;
      formDistrict.year = data;

      getDataByGrade();
      getDataByDistrict();
    });
    $rootScope.globalWatch.watchReportDef = watch;
    $scope.$on("$destroy", function () {
      watch();
    });

  }])
  .controller('reportAadtController', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope) {
    var form = $scope.form = {
      year: $rootScope.globalYear,
      offset: 1,
      count: 10
    };
    function list() {
      reportIF.aadtGetlist(form).success(function (data) {
        if (data.status == 0) {
          $scope.listData = data.data.list || [];
          form.total = data.data.total;
        }
      });
    }

    $scope.pageChanged = function () {
      list();
    };
    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '需首先在「数据导入」中完成「数据库」的动态数据导入，并在「分析评价」中完成「数据库」的PQI、RQI、ESAL的计算后方可查看';

      form.year = data;
      form.offset = 1;
      list();
    });
    $rootScope.globalWatch.watchReportAadt = watch;
    $scope.$on("$destroy", function () {
      watch();
    });
  }])
  .controller('roadAdviceMaintainController', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', '$route', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope, $route) {
    var getTableName = $route.current.tableName;
    var moduleName = $route.current.moduleName;
    var form = $scope.form = {
      year: $rootScope.globalYear
    };
    // 根据横坐标名字，获取数据，解决有些字段缺失问题。
    function getGradeChartData(list, params) {
      return Math.max.apply(null, list.map(function (v, k) {
        return params == v.sCategory ? v.drivewayArea_s : 0
      }))
    }
    function initGradeChart(data) {
      var xAxis = [];
      var gradeChartData = [];
      var legend = [];
      // 郊区或者市区
      angular.forEach(data, function (v, k) {
        var chartData = [];
        // 路类型
        angular.forEach(v.list, function (value, key) {
          if (value.name == '汇总') {
            return;
          }
          legend.push(value.name);
          var areaData = [];
          var xAxis2 = value.list.map(function(va) {
            return va.sCategory;
          });
          if (xAxis2.length > xAxis.length) {
            xAxis = xAxis2;
          }
          // 路类型具体数据
          angular.forEach(xAxis, function (va, ke) {
            if (ke === xAxis.length - 1) {
              // 去掉总计
              return;
            }
            // 横坐标
            var total = value.list[value.list.length - 1].drivewayArea_s;
            var ad = getGradeChartData(value.list, va);
            var result = ((ad / total) * 100).toFixed(2);
            areaData.push(result)
          });
          chartData.push({
            name: value.name,
            type: 'bar',
            data: areaData,
            itemStyle: {
              normal: {
                label: {
                  show: true,
                  position: 'top'
                }
              }
            }
          });
        });

        gradeChartData.push({
          xAxis: [{
            type: 'category',
            axisLabel: {
              interval: 0,//横轴信息全部显示  
              formatter: function (value) {
                var result = "";//拼接加\n返回的类目项
                var maxLength = 5;//每项显示文字个数
                var valLength = value.length;//X轴类目项的文字个数
                var rowNumber = Math.ceil(valLength / maxLength); //类目项需要换行的行数
                if (rowNumber > 1)//如果文字大于3,
                {
                  for (var i = 0; i < rowNumber; i++) {
                    var temp = "";//每次截取的字符串
                    var start = i * maxLength;//开始截取的位置
                    var end = start + maxLength;//结束截取的位置
                    temp = value.substring(start, end) + "\n";
                    result += temp; //拼接生成最终的字符串
                  }
                  return result;
                }
                else {
                  return value;
                }
              }
            },          
            data: xAxis.slice(0, xAxis.length -1) // 去掉总计
          }],
          district: v.name,
          legend: {
            data: legend
          },
          grid: {
            y2: 100  //增加柱形图纵向的高度
          },
          yAxis: [{
            type: 'value',
            name: '长度百分比(%)'
          }],
          series: chartData
        })
      });
      $scope.gradeChartData = gradeChartData;
    }

    function initDistrictChart(data) {
      var xAxis = [];
      var chartData = [];
      angular.forEach(data, function (v, k) {
        angular.forEach(v.list, function (value, key) {
          if (value.district != '总计') {
            xAxis.push(value.district);
            chartData.push(value.roadCnt);
          }

        });
      });
      $scope.districtChartData = {
        xAxis: [{
          type: 'category',
          data: xAxis
        }],
        yAxis: [{
          type: 'value',
          name: '项目数（段）'
        }],
        series: [{
          type: 'bar',
          data: chartData,
          itemStyle: {
            normal: {
              label: {
                show: true,
                position: 'top'
              }
            }
          }
        }]
      };
    }

    function initDistrictChart2(data) {
      var xAxis = [];
      var chartData = [];
      angular.forEach(data, function (v, k) {
        angular.forEach(v.list, function (value, key) {
          if (value.district != '总计') {
            xAxis.push(value.district);
            chartData.push(value.drivewayArea_s);
          }

        });
      });
      $scope.districtChartData2 = {
        xAxis: [{
          type: 'category',
          data: xAxis
        }],
        yAxis: [{
          type: 'value',
          name: '长度(km)'
        }],
        series: [{
          type: 'bar',
          data: chartData,
          itemStyle: {
            normal: {
              label: {
                show: true,
                position: 'top'
              }
            }
          }
        }]
      };
    }

    function getDataByGrade() {
      reportIF.getDataByGrade(form, moduleName, getTableName).success(function (data) {
        if (data.status == 0) {
          $scope.gradeList = data.data || [];
          initGradeChart(data.data || []);
        }
      });
    }
    function getDataByDistrict() {
      reportIF.getDataByDistrict(form, moduleName, getTableName).success(function (data) {
        if (data.status == 0) {
          $scope.districtList = data.data || [];
          initDistrictChart(data.data || []);
          initDistrictChart2(data.data || []);
        }
      });
    }

    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      form.year = data;
      $scope.pageNoDataTip = '需首先在「分析评价」中完成「数据库」的决策树方案计算后方可查看';

      getDataByGrade();
      getDataByDistrict();
    });
    $rootScope.globalWatch.watchReportAadt = watch;
    $scope.$on("$destroy", function () {
      watch();
    });

  }])
  .controller('roadAdviceMaintain2Controller', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', '$route', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope, $route) {
    var form = $scope.form = {
      year: $rootScope.globalYear,
      type: 1,
      offset: 1,
      extra: 0,
      count: 10
    };

    var form2 = $scope.form2 = {
      year: $rootScope.globalYear,
      type: 2,
      offset: 1,
      extra: 0,
      count: 10
    };

    var form3 = $scope.form3 = {
      year: $rootScope.globalYear,
      type: 1,
      extra: 1,
      offset: 1,
      count: 10
    };

    var form4 = $scope.form4 = {
      year: $rootScope.globalYear,
      type: 2,
      extra: 1,
      offset: 1,
      count: 10
    };

    function list(getForm) {
      reportIF.getMaintainList(getForm).success(function (data) {
        if (data.status == 0) {
          getForm.data = data.data.list || [];
          getForm.total = data.data.total;
        }
      });
    }

    $scope.pageChanged = function () {
      delete form.data;
      list(form);
    };

    $scope.pageChanged2 = function () {
      delete form2.data;
      list(form2);
    };

    $scope.pageChanged3 = function () {
      delete form3.data;
      list(form3);
    };

    $scope.pageChanged4 = function () {
      delete form4.data;
      list(form4);
    };

    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '需首先在「分析评价」中完成「数据库」的决策树方案计算后方可查看';

      form.offset = 1;
      form2.offset = 1;
      form3.offset = 1;
      form4.offset = 1;

      delete form.data;
      delete form2.data;
      delete form3.data;
      delete form4.data;

      form.year = data;
      form2.year = data;
      form3.year = data;
      form4.year = data;

      list(form);
      list(form2);
      list(form3);
      list(form4);
    });
    $rootScope.globalWatch.watchRoadAdviceMaintain2 = watch;
    $scope.$on("$destroy", function () {
      watch();
    });

  }])
  .controller('roadAdviceListController', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', '$route', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope, $route) {
    var form = $scope.form = {
      year: $rootScope.globalYear,
    };

    function list(getForm) {
      reportIF.getAdvicelist(getForm).success(function (data) {
        if (data.status == 0) {
          getForm.data = data.data || [];
          getForm.total = data.data.total;
        }
      });
    }
    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '需首先在「分析评价」中完成「数据库」的路面行驶质量指数RQI的计算后方可查看';

      form.year = data;

      list(form);
    });
    $rootScope.globalWatch.watchRoadAdviceMaintain2 = watch;
    $scope.$on("$destroy", function () {
      watch();
    });
  }])
  .controller('roadPlanPerformanceController', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', '$route', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope, $route) {
    var getTableName = $route.current.tableName; // 有相似模块
    var moduleName = $route.current.moduleName;

    var formGrade = $scope.formGrade = {
      year: $rootScope.globalYear
    };

    // 市区
    var formDistrictDowntown = $scope.formDistrictDowntown = {
      year: $rootScope.globalYear,
      type: 1
    };


    function getDataByDistrictDowntown() {
      reportIF.getDataByDistrict(formDistrictDowntown, moduleName, getTableName).success(function (data) {
        if (data.status == 0) {
          $scope.districtListDowntown = data.data || [];
        }
      });
    }

    function countTableYears() {
      var currentYear = Number(angular.copy($rootScope.globalYear));
      for (var i = currentYear + 1; i <= currentYear + 5; i++) {
        $scope.tableYears.push(i)
      }
    }

    function initGradeChart(data, type) {
      var xAxis = $scope.tableYears;
      var legend = ['一级公路', '二级公路', '三级公路', '四级公路', '等外'];
      var chartData = legend.map(function (v) {
        return {
          name: v,
          type: 'bar',
          data: [],
          itemStyle: {
            normal: {
              label: {
                show: true,
                position: 'top'
              }
            }
          }
        }
      });
      // 郊区或者市区
      angular.forEach(data, function (v, k) {
        angular.forEach(v, function (value, key) {
          if (value.label == '一级公路') {
            chartData[0].data.push(type == 'cnt' ? value.roadCnt : value.drivewayArea);
          } else if (value.label == '二级公路') {
            chartData[1].data.push(type == 'cnt' ? value.roadCnt : value.drivewayArea);
          } else if (value.label == '三级公路') {
            chartData[2].data.push(type == 'cnt' ? value.roadCnt : value.drivewayArea);
          } else if (value.label == '四级公路') {
            chartData[3].data.push(type == 'cnt' ? value.roadCnt : value.drivewayArea);
          } else if (value.label == '等外') {
            chartData[4].data.push(type == 'cnt' ? value.roadCnt : value.drivewayArea);
          }
        });

      });
      console.log(chartData, 'chartData');
      var gradeChartData = {
        xAxis: [{
          type: 'category',
          data: xAxis
        }],
        legend: {
          data: legend
        },
        yAxis: [{
          type: 'value',
          name: type == 'cnt' ? '路段数' : '长度(km)'
        }],
        series: chartData
      }
      return gradeChartData
    }

    function getDataByGrade(form, callback) {
      reportIF.getDataByGrade(form, moduleName, getTableName).success(function (data) {
        if (data.status == 0) {
          callback(initGradeChart(data.data || [], form.chartType))
        }
      });
    }

    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '需首先在「分析评价」中完成「数据库」的各项中长期养护规划指标的计算后方可查看';

      formGrade.year = $rootScope.globalYear;

      $scope.tableYears = [];

      formDistrictDowntown.year = $rootScope.globalYear;

      countTableYears();

      getDataByDistrictDowntown();

      // 市区路段路
      getDataByGrade({
        year: $rootScope.globalYear,
        type: 1,
        chartType: 'cnt'
      }, function (data) {
        $scope.gradeChartData = data;
      });

      // 市区面积
      getDataByGrade({
        year: $rootScope.globalYear,
        type: 1,
        chartType: 'area'
      }, function (data) {
        $scope.gradeChartData2 = data;
      });
    });
    $rootScope.globalWatch.watchRoadPlanPerformance = watch;
    $scope.$on("$destroy", function () {
      watch();
    });
  }])
  .controller('roadPlanPerformance2Controller', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', '$route', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope, $route) {
    var getTableName = $route.current.tableName; // 有相似模块
    var moduleName = $route.current.moduleName;

    var formUsage = $scope.formGrade = {
      year: $rootScope.globalYear
    };

    function filterArray(array, num) {
      var temp = []
      return array.map(function (v, k) {
        var item = v.substring(0, v.indexOf('.') + num);
        return temp.indexOf(item) == -1 ? temp.push(item) : false
      })
    }

    function initChart(data, type) {
      var xAxis = $scope.tableYears;
      var chartData = [];
      // 郊区或者市区
      angular.forEach(data, function (v, k) {
        var legend = [];
        if (v.name == '') {
          return;
        }
        var chart = {
          xAxis: [{
            type: 'category',
            data: xAxis
          }],
          legend: {
            data: null
          },
          type: v.name,
          yAxis: [{
            type: 'value',
            name: type,
            scale: true
          }],
          series: []
        }
        var v0Array = [];
        var v1Array = [];
        var v2Array = [];
        var v3Array = [];
        var v4Array = [];
        var v5Array = [];
        angular.forEach(v.list, function (value, key) {
          if (value.name == '合计') {
            return;
          }
          legend.push(value.name)
          var series = {
            name: value.name,
            type: 'line',
            data: [],
            itemStyle: {
              normal: {
                label: {
                  show: true,
                  position: 'top'
                }
              }
            }
          }


          angular.forEach(value.list, function (va, ke) {
            if (va.tag == type) {
              v0Array.push(va.v0);
              v1Array.push(va.v1);
              v2Array.push(va.v2);
              v3Array.push(va.v3);
              v4Array.push(va.v4);
              v5Array.push(va.v5);
              series.data.push(va.v0);
              series.data.push(va.v1);
              series.data.push(va.v2);
              series.data.push(va.v3);
              series.data.push(va.v4);
              series.data.push(va.v5);
            }
          });
          chart.series.push(series);
        });
        // 处理图表label重叠情况
        var totalArray = [v0Array, v1Array, v2Array, v3Array, v4Array, v5Array];
        for (var y = 0; y < totalArray.length; y++) {
          // 针对报表数据最大值和最小值小于1的，保留小数点后面1位
          if ((Math.max.apply(Math, totalArray[y]) - Math.min.apply(Math, totalArray[y])) < 1) {
            var fa = filterArray(totalArray[y], 2);
            for (var i = 0; i < fa.length; i++) {
              if (!fa[i]) {
                delete chart.series[i].itemStyle;
              }

            }
          } else {
            // 否则不保留小数点
            var fa = filterArray(totalArray[y], 0);
            for (var i = 0; i < fa.length; i++) {
              if (!fa[i]) {
                delete chart.series[i].itemStyle;
              }

            }
          }
        }
        chart.legend.data = legend;
        chartData.push(chart);
      });
      return chartData;
    }

    function getPerformanceUsage() {
      reportIF.getPerformanceUsage(formUsage, moduleName, getTableName).success(function (data) {
        if (data.status == 0) {
          $scope.pageData = data.data || [];
          $scope.chartData = initChart(data.data || [], 'RQI');
          $scope.chartData2 = initChart(data.data || [], 'PCI');
          $scope.chartData3 = initChart(data.data || [], 'DEF');
        }
      });
    }

    function countTableYears() {
      var currentYear = Number(angular.copy($rootScope.globalYear));
      for (var i = currentYear; i <= currentYear + 5; i++) {
        $scope.tableYears.push(i)
      }
    }
    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '需首先在「分析评价」中完成「数据库」的各项中长期养护规划指标的计算后方可查看';

      formUsage.year = $rootScope.globalYear;
      $scope.tableYears = [];

      countTableYears();
      getPerformanceUsage();
    });
    $rootScope.globalWatch.watchRoadPlanPerformance2 = watch;
    $scope.$on("$destroy", function () {
      watch();
    });
  }])
  .controller('compareDistrictController', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope) {
    var districtType = $scope.districtType = $routeParams.districtType;
    var district = $scope.district = $routeParams.district;

    var form = $scope.form = {
      year: $rootScope.globalYear,
      districtName: district
    };

    function list() {
      reportIF.districtCompare(form).success(function (data) {
        if (data.status == 0) {
          form.data = data.data || [];
        }
      });
    }

    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '暂时没有该年份的数据，请导入' + $rootScope.globalYear + '年份的动静态数据';

      form.data = [];
      form.year = data;
      list();
    });
    $rootScope.globalWatch.watchCompareDistrict = watch;
    $scope.$on("$destroy", function () {
      watch();
    });

  }])
  .controller('districtController', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope) {
    var district = $scope.district = $routeParams.district || $rootScope.globalUserinfo.currentUser.district;

    var form = $scope.form = {
      year: $rootScope.globalYear,
      districtName: district
    };

    function list() {
      reportIF.summaryGetlist(form).success(function (data) {
        if (data.status == 0) {
          form.data = data.data || [];
        }
      });
    }

    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '暂时没有该年份的数据，请导入' + $rootScope.globalYear + '年份的动静态数据';

      form.data = [];
      form.year = data;
      list();
    });
    $rootScope.globalWatch.watchCompareDistrict = watch;
    $scope.$on("$destroy", function () {
      watch();
    });
  }])
  .controller('districtRqiController', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', '$route', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope, $route) {
    var getTableName = $scope.getTableName = $route.current.tableName; // 有相似模块
    var moduleName = $route.current.moduleName;
    var district = $scope.district = $routeParams.district;

    if (getTableName == 'rqi') {
      $scope.newTableName = 'RQI';
    } else if (getTableName == 'pci') {
      $scope.newTableName = 'PCI';
    }
    var gradeList = $scope.gradeList = {};

    var formGrade = $scope.formGrade = {
      year: $rootScope.globalYear,
      districtName: district
    };

    var pageData = {};

    // 黄浦区检测道路RQI按面积统计图
    function initGradeChart(data) {
      var xAxis = ['优秀', '良好', '合格', '不合格'];
      var chartData = [];
      var legend = [];
      angular.forEach(data, function (v, k) {
        if (v.grade == '总计') {
          return;
        }
        legend.push(v.grade);
        chartData.push({
          name: v.grade,
          type: 'bar',
          stack: 'group',
          data: [Number(v.youxiu_area_s).toFixed(2), Number(v.lianghao_area_s).toFixed(2), Number(v.jige_area_s).toFixed(2), Number(v.bujige_area_s).toFixed(2)]
        });
      });
      $scope.gradeChartData = {
        xAxis: [{
          type: 'category',
          data: xAxis
        }],
        legend: {
          data: legend
        },
        yAxis: [{
          type: 'value',
          name: '长度(km)'
        }],
        series: chartData
      };

    }

    // 黄浦区各等级检测道路RQI比较图
    function initGradeChart2(data) {
      console.log(data);
      var xAxis = [];
      var chartData = [{
        type: 'bar',
        data: [],
        itemStyle: {
          normal: {
            label: {
              show: true,
              position: 'top'
            }
          }
        }
      }];
      angular.forEach(data, function (v, k) {
        xAxis.push(v.grade);
        if (getTableName == 'rqi') {
          chartData[0].data.push(Number(v.avg_rqi).toFixed(2));
        } else if (getTableName == 'pci') {
          chartData[0].data.push(Number(v.avg_pci).toFixed(2));
        }
      });

      $scope.gradeChartData2 = {
        xAxis: [{
          type: 'category',
          data: xAxis
        }],
        legend: {
          data: []
        },
        yAxis: [{
          type: 'value',
          name: '长度(km)'
        }],
        series: chartData
      };
    }
    function getDataByGrade() {
      reportIF.getDataByGradeInDistrict(formGrade, moduleName, getTableName).success(function (data) {
        if (data.status == 0) {
          $scope.gradeList = data.data || [];
          initGradeChart(data.data || [])
          initGradeChart2(data.data || [])
        }
      });
    }

    function pageInfo() {
      reportIF.pageInfo().success(function (data) {
        if (data.status == 0) {
          pageData = data.data;
          getDataByGrade();
        }
      });
    }
    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '需首先在「数据导入」中完成「数据库」的动态数据导入，并在「分析评价」中完成「数据库」的PQI、RQI、ESAL的计算后方可查看';

      formGrade.year = data;

      pageInfo();
    });
    $rootScope.globalWatch.watchDistrictRqi = watch;
    $scope.$on("$destroy", function () {
      watch();
    });
  }])
  .controller('districtDefController', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', '$route', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope, $route) {
    var getTableName = $scope.getTableName = $route.current.tableName; // 有相似模块
    var moduleName = $route.current.moduleName;
    var district = $scope.district = $routeParams.district;

    $scope.newTableName = 'DEF';
    var gradeList = $scope.gradeList = {};
    var formGrade = $scope.formGrade = {
      year: $rootScope.globalYear,
      districtName: district
    };

    // 黄浦区检测道路RQI按面积统计图
    function initGradeChart(data) {
      var xAxis = ['足够', '临界', '不足'];
      var chartData = [];
      var legend = [];
      angular.forEach(data, function (v, k) {
        if (v.grade == '总计') {
          return;
        }
        legend.push(v.grade);
        chartData.push({
          name: v.grade,
          type: 'bar',
          stack: 'group',
          data: [Number(v.zugou_area_s).toFixed(2), Number(v.linjie_area_s).toFixed(2), Number(v.buzu_area_s).toFixed(2)]
        });
      });
      $scope.gradeChartData = {
        xAxis: [{
          type: 'category',
          data: xAxis
        }],
        legend: {
          data: legend
        },
        yAxis: [{
          type: 'value',
          name: '面积：万㎡'
        }],
        series: chartData
      };
    }

    // 黄浦区各等级检测道路RQI比较图
    function initGradeChart2(data) {
      var xAxis = [];
      var chartData = [{
        type: 'bar',
        data: [],
        itemStyle: {
          normal: {
            label: {
              show: true,
              position: 'top'
            }
          }
        }
      }];
      angular.forEach(data, function (v, k) {
        if (v.grade == '总计') {
          return;
        }
        xAxis.push(v.grade);
        chartData[0].data.push(v.linjie);
      });

      $scope.gradeChartData2 = {
        xAxis: [{
          type: 'category',
          data: xAxis
        }],
        legend: {
          data: []
        },
        yAxis: [{
          type: 'value',
          name: '面积：万㎡'
        }],
        series: chartData
      };
    }

    function getDataByGrade() {
      reportIF.getDataByGradeInDistrict(formGrade, moduleName, getTableName).success(function (data) {
        if (data.status == 0) {
          $scope.gradeList = data.data || [];
          initGradeChart(data.data || [])
          initGradeChart2(data.data || [])
        }
      });
    }

    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '需首先在「数据导入」中完成「数据库」的动态数据导入，并在「分析评价」中完成「数据库」的PQI、RQI、ESAL的计算后方可查看';

      formGrade.year = data;

      getDataByGrade();
    });
    $rootScope.globalWatch.watchDistrictDef = watch;
    $scope.$on("$destroy", function () {
      watch();
    });

  }])
  .controller('districtAadtController', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope) {
    var district = $scope.district = $routeParams.district;

    var form = $scope.form = {
      year: $rootScope.globalYear,
      districtName: district,
      offset: 1,
      count: 10
    };
    function list() {
      reportIF.aadtGetlistInDistrict(form).success(function (data) {
        if (data.status == 0) {
          $scope.listData = data.data.list || [];
          form.total = data.data.total;
        }
      });
    }

    $scope.pageChanged = function () {
      list();
    };

    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '需首先在「数据导入」中完成「数据库」的动态数据导入，并在「分析评价」中完成「数据库」的PQI、RQI、ESAL的计算后方可查看';

      form.year = data;
      form.offset = 1;

      list();
    });
    $rootScope.globalWatch.watchDistrictAadt = watch;
    $scope.$on("$destroy", function () {
      watch();
    });
  }])
  .controller('districtRoadAdviceMaintainController', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', '$route', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope, $route) {
    var getTableName = $route.current.tableName;
    var moduleName = $route.current.moduleName;
    var district = $scope.district = $routeParams.district;
    var form = $scope.form = {
      year: $rootScope.globalYear,
      districtName: district
    };

    function initGradeChart(data) {
      var gradeChartData = {
        xAxis: [{
          type: 'category',
          axisLabel: {
            interval: 0,//横轴信息全部显示  
            formatter: function (value) {
              var result = "";//拼接加\n返回的类目项
              var maxLength = 5;//每项显示文字个数
              var valLength = value.length;//X轴类目项的文字个数
              var rowNumber = Math.ceil(valLength / maxLength); //类目项需要换行的行数
              if (rowNumber > 1)//如果文字大于3,
              {
                for (var i = 0; i < rowNumber; i++) {
                  var temp = "";//每次截取的字符串
                  var start = i * maxLength;//开始截取的位置
                  var end = start + maxLength;//结束截取的位置
                  temp = value.substring(start, end) + "\n";
                  result += temp; //拼接生成最终的字符串
                }
                return result;
              }
              else {
                return value;
              }
            }
          },
          data: []
        }],
        legend: {
          data: []
        },
        grid: {
          y2: 100  //增加柱形图纵向的高度
        },
        yAxis: [{
          type: 'value',
          name: '长度(km)'
        }],
        series: []
      };
      // 郊区或者市区
      angular.forEach(data, function (v, k) {
        if (v.name == '总计') {
          return;
        }
        gradeChartData.legend.data.push(v.name);
        var chartData = {
          name: v.name,
          type: 'bar',
          data: [],
          itemStyle: {
            normal: {
              label: {
                show: true,
                position: 'top'
              }
            }
          }
        };
        // 路类型
        angular.forEach(v.list, function (value, key) {
          if (value.sCategory != '总计') {
            if (!gradeChartData.xAxis[0].data.includes(value.sCategory)) {
              gradeChartData.xAxis[0].data.push(value.sCategory);
            }
            chartData.data.push(value.drivewayArea_s)
          }
        });
        gradeChartData.series.push(chartData);
      });
      console.log(gradeChartData);
      $scope.gradeChartData = gradeChartData;
    }

    function getDataByGrade() {
      reportIF.getDataByGradeInDistrict(form, moduleName, getTableName).success(function (data) {
        if (data.status == 0) {
          $scope.gradeList = data.data || [];
          initGradeChart(data.data || []);
        }
      });
    }

    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '需首先在「分析评价」中完成「数据库」的决策树方案计算后方可查看';

      form.year = data;

      getDataByGrade();
    });
    $rootScope.globalWatch.watchDistrictRoadAdviceMaintain = watch;
    $scope.$on("$destroy", function () {
      watch();
    });

  }])
  .controller('districtRoadAdviceMaintain2Controller', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', '$route', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope, $route) {
    var district = $scope.district = $routeParams.district;
    var form = $scope.form = {
      year: $rootScope.globalYear,
      type: 1,
      offset: 1,
      count: 10,
      districtName: district
    };

    var form2 = $scope.form2 = {
      year: $rootScope.globalYear,
      type: 2,
      offset: 1,
      count: 10,
      extra: 1,
      districtName: district
    };

    function list(getForm) {
      reportIF.getMaintainListInDistrict(getForm).success(function (data) {
        if (data.status == 0) {
          getForm.data = data.data.list || [];
          getForm.total = data.data.total;
        }
      });
    }

    $scope.pageChanged = function () {
      delete form.data;
      list(form);
    };

    $scope.pageChanged2 = function () {
      delete form2.data;
      list(form2);
    };

    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '需首先在「分析评价」中完成「数据库」的决策树方案计算后方可查看';

      form.offset = 1;
      form2.offset = 1;

      delete form.data;
      delete form2.data;

      form.year = data;
      form2.year = data;

      list(form);
      list(form2);
    });
    $rootScope.globalWatch.watchDistrictRoadAdviceMaintain2 = watch;
    $scope.$on("$destroy", function () {
      watch();
    });
  }])
  .controller('districtRroadAdviceListController', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', '$route', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope, $route) {
    var district = $scope.district = $routeParams.district;
    var form = $scope.form = {
      year: $rootScope.globalYear,
      offset: 1,
      count: 10,
      districtName: district
    };
    function list(getForm) {
      reportIF.getAdvicelistInDistrict(getForm).success(function (data) {
        if (data.status == 0) {
          getForm.data = data.data.list || [];
          getForm.total = data.data.total;
        }
      });
    }
    $scope.pageChanged = function () {
      delete form.data;
      list(form);
    };
    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '需首先在「分析评价」中完成「数据库」的路面行驶质量指数RQI的计算后方可查看';

      form.year = data;

      list(form);
    });
    $rootScope.globalWatch.watchDistrictRroadAdviceList = watch;
    $scope.$on("$destroy", function () {
      watch();
    });
  }])
  .controller('districtRoadPlanPerformanceController', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', '$route', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope, $route) {
    var district = $scope.district = $routeParams.district;
    var getTableName = $route.current.tableName; // 有相似模块
    var moduleName = $route.current.moduleName;

    var formGrade = $scope.formGrade = {
      year: $rootScope.globalYear
    };

    // 市区
    var formDistrict = $scope.formDistrict = {
      year: $rootScope.globalYear,
      districtName: district
    };


    function initGradeChart(data, type) {
      console.log(data);
      var xAxis = $scope.tableYears;
      var legend = [];
      var chartData = [];
      // 郊区或者市区
      angular.forEach(data, function (v, k) {
        if (v.grade != '总计') {
          legend.push(v.grade);
          var obj = {
            name: v.grade,
            type: 'bar',
            stack: 'group',
            data: []
          };
          if (type == 'cnt') {
            obj.data.push(v.roadCnt1, v.roadCnt2, v.roadCnt3, v.roadCnt4, v.roadCnt5);
          } else {
            obj.data.push(v.drivewayArea1, v.drivewayArea2, v.drivewayArea3, v.drivewayArea4, v.drivewayArea5);
          }
          chartData.push(obj);
        }
      });

      var gradeChartData = {
        xAxis: [{
          type: 'category',
          data: xAxis
        }],
        legend: {
          data: legend
        },
        yAxis: [{
          type: 'value',
          name: type == 'cnt' ? '路段数' : '长度(km)'
        }],
        series: chartData
      }
      return gradeChartData
    }

    function getDataByGrade() {
      reportIF.getDataByGradeInDistrict(formDistrict, moduleName, getTableName).success(function (data) {
        if (data.status == 0) {
          $scope.gradeList = data.data || [];
          $scope.gradeChartData = initGradeChart(data.data, 'cnt');
          $scope.gradeChartData2 = initGradeChart(data.data, 'area');
        }
      });
    }

    function countTableYears() {
      var currentYear = Number(angular.copy($rootScope.globalYear));
      for (var i = currentYear + 1; i <= currentYear + 5; i++) {
        $scope.tableYears.push(i)
      }
    }

    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '需首先在「分析评价」中完成「数据库」的各项中长期养护规划指标的计算后方可查看';

      formGrade.year = $rootScope.globalYear;

      formDistrict.year = $rootScope.globalYear;

      $scope.tableYears = [];
      countTableYears();
      getDataByGrade();
    });
    $rootScope.globalWatch.watchDistrictRoadPlanPerformance = watch;
    $scope.$on("$destroy", function () {
      watch();
    });

  }])
  .controller('districtRoadPlanPerformance2Controller', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', '$route', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope, $route) {
    var district = $scope.district = $routeParams.district;
    var getTableName = $route.current.tableName; // 有相似模块
    var moduleName = $route.current.moduleName;

    var formUsage = $scope.formGrade = {
      year: $rootScope.globalYear,
      districtName: district
    };

    function filterArray(array, num) {
      var temp = []
      return array.map(function (v, k) {
        var item = v.substring(0, v.indexOf('.') + num);
        return temp.indexOf(item) == -1 ? temp.push(item) : false
      })
    }

    function initChart(data, type) {
      var legend = [];
      var chartData = {
        xAxis: [{
          type: 'category',
          data: $scope.tableYears
        }],
        legend: {
          data: []
        },
        yAxis: [{
          type: 'value',
          scale: true,
          name: type
        }],
        series: []
      };

      var v0Array = [];
      var v1Array = [];
      var v2Array = [];
      var v3Array = [];
      var v4Array = [];
      var v5Array = [];
      // 郊区或者市区
      angular.forEach(data, function (v, k) {
        legend.push(v.name);

        angular.forEach(v.list, function (value, key) {
          if (value.tag == type) {
            v0Array.push(value.v0);
            v1Array.push(value.v1);
            v2Array.push(value.v2);
            v3Array.push(value.v3);
            v4Array.push(value.v4);
            v5Array.push(value.v5);
            chartData.series.push({
              name: v.name,
              type: 'line',
              itemStyle: {
                normal: {
                  label: {
                    show: true,
                    position: 'top'
                  }
                }
              },
              data: [value.v0, value.v1, value.v2, value.v3, value.v4, value.v5]
            });
          }
        });
        // 处理图表label重叠情况
        var totalArray = [v0Array, v1Array, v2Array, v3Array, v4Array, v5Array];
        for (var y = 0; y < totalArray.length; y++) {
          // 针对报表数据最大值和最小值小于1的，保留小数点后面1位
          if ((Math.max.apply(Math, totalArray[y]) - Math.min.apply(Math, totalArray[y])) < 1) {
            var fa = filterArray(totalArray[y], 2);
            for (var i = 0; i < fa.length; i++) {
              if (!fa[i]) {
                delete chartData.series[i].itemStyle;
              }

            }
          } else {
            // 否则不保留小数点
            var fa = filterArray(totalArray[y], 0);
            for (var i = 0; i < fa.length; i++) {
              if (!fa[i]) {
                delete chartData.series[i].itemStyle;
              }

            }
          }
        }
      });
      chartData.legend.data = legend;
      return chartData;
    }

    function getPerformanceUsage() {
      reportIF.getPerformanceUsageInDistrict(formUsage, moduleName, getTableName).success(function (data) {
        if (data.status == 0) {
          $scope.pageData = data.data || [];
          $scope.chartData = initChart(data.data || [], 'RQI');
          $scope.chartData2 = initChart(data.data || [], 'PCI');
          $scope.chartData3 = initChart(data.data || [], 'DEF');
        }
      });
    }

    function countTableYears() {
      var currentYear = Number(angular.copy($rootScope.globalYear));
      for (var i = currentYear; i <= currentYear + 5; i++) {
        $scope.tableYears.push(i)
      }
    }

    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '需首先在「分析评价」中完成「数据库」的各项中长期养护规划指标的计算后方可查看';

      formUsage.year = $rootScope.globalYear;
      $scope.tableYears = [];
      countTableYears();
      getPerformanceUsage();
    });
    $rootScope.globalWatch.watchDistrictRoadPlanPerformance2 = watch;
    $scope.$on("$destroy", function () {
      watch();
    });
  }])
  .controller('districtRoadPlanPerformance3Controller', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', '$route', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope, $route) {
    var district = $scope.district = $routeParams.district;
    var getTableName = $route.current.tableName; // 有相似模块
    var moduleName = $route.current.moduleName;

    var formUsage = $scope.formGrade = {
      year: $rootScope.globalYear,
      districtName: district
    };

    function getPerformanceUsage() {
      reportIF.getPCILess75List(formUsage).success(function (data) {
        if (data.status == 0) {
          $scope.pageData = data.data || [];
        }
      });
    }
    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '需首先在「分析评价」中完成「数据库」的各项中长期养护规划指标的计算后方可查看';

      formUsage.year = $rootScope.globalYear;

      getPerformanceUsage();
    });
    $rootScope.globalWatch.watchDistrictRoadPlanPerformance3 = watch;
    $scope.$on("$destroy", function () {
      watch();
    });
  }])
  .controller('dynamicListController', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', '$route', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope, $route) {
    var district = $scope.district = $routeParams.district;
    var form = $scope.form = {
      year: $rootScope.globalYear,
      offset: 1,
      count: 10,
      districtName: district
    };
    function list(getForm) {
      reportIF.getDynamicList(getForm).success(function (data) {
        if (data.status == 0) {
          getForm.data = data.data.list || [];
          getForm.total = data.data.total;
        }
      });
    }
    $scope.pageChanged = function () {
      delete form.data;
      list(form);
    };
    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '暂时没有该年份的数据，请导入' + $rootScope.globalYear + '年份的动静态数据';

      form.year = data;
      delete form.data;
      list(form);
    });
    $rootScope.globalWatch.watchDynamicList = watch;
    $scope.$on("$destroy", function () {
      watch();
    });
  }])
  .controller('districtSummaryListController', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', '$route', function($scope, $routeParams, reportIF, Notify, Alert ,$rootScope, $route) {
    $scope.districtType = $routeParams.districtType || $rootScope.districtType;
    $scope.district = $routeParams.district || $rootScope.globalUserinfo.currentUser.district;
    var form = $scope.form = {
        year: $rootScope.globalYear,
        offset: 1,
        count: 10,
        district: $scope.district,
    };

    $scope.pageChanged = function() {
        list();
    };

    function list() {
        delete form.data;
        reportIF.summaryList(form).success(function(data) {
            if (data.status == 0) {
                form.data = data.data.list || [];
                form.total = data.data.total;
            }
        });
    }
    // 检测数据
    var watch = $rootScope.$watch('globalYear',function(data){
        $scope.pageNoDataTip = '需首先在「分析评价」中完成「数据库」的路段综合分析后方可查看';
        form.year = data;
        list();
    });
    $rootScope.globalWatch.watchDistrictSummaryListDetail = watch;
    $scope.$on("$destroy", function() {
        watch();
    });
}])
  .controller('districtSummaryDetailController', ['$scope', '$routeParams', 'reportIF', 'Notify', 'Alert', '$rootScope', '$route', function ($scope, $routeParams, reportIF, Notify, Alert, $rootScope, $route) {
    var sid =  $routeParams.sid;
    var district = $scope.district = $routeParams.district;
    var form = $scope.form = {
      year: $rootScope.globalYear,
      offset: 1,
      count: 1
    };

    var taskForm = $scope.taskForm = {
      order: 'asc',
      offset: 1,
      count: 10,
      workerType: 14
    };

    var pageData = $scope.pageData = {};

    function initChart(data, type) {
      var xAxis = [];
      var chartData = {
        xAxis: [{
          type: 'category',
          data: [1, 2, 3, 4, 5]
        }],
        legend: {
          data: []
        },
        yAxis: [{
          type: 'value',
          name: ''
        }],
        series: [{
          name: type,
          type: 'line',
          stack: 'group',
          data: data || [],
          itemStyle: {
            normal: {
              label: {
                show: true,
                position: 'top'
              }
            }
          }
        }]
      };
      angular.forEach(data, function (v, k) {
        xAxis.push(k + 1);
      });
      chartData.xAxis[0].data = xAxis;
      return chartData;
    }

    function list(getForm) {
      reportIF.summaryDetail(getForm).success(function (data) {
        if (data.status == 0) {
          if (data.data && data.data.detail) {
            pageData = $scope.pageData = data.data.detail;
            pageData.sContent.pci_list = initChart(pageData.sContent.pci_list, 'PCI');
            pageData.sContent.rqi_list = initChart(pageData.sContent.rqi_list, 'RQI');
            form.total = data.data.total;
          }
        }
      });
    }
    $scope.download = function () {
      if (!pageData.download_url) {
        Alert.alert('请先计算路段综合分析！', 'danger');
        return;
      }
      window.location.href = pageData.download_url;
    }

    $scope.pageChanged = function () {
      list(form);
    };
    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      $scope.pageNoDataTip = '需首先在「分析评价」中完成「数据库」的路段综合分析后方可查看';

      form.year = data;
      form.offset = 1;
      form.sid = sid;
      form.district = district;
      list(form);
    });
    $rootScope.globalWatch.watchDistrictSummaryDetail = watch;
    $scope.$on("$destroy", function () {
      watch();
    });
  }])
  // 报表侧边栏
  .value('reportSidebars', (function () {
    var sidebars = [
      {
        label: '概况',
        parentType: 'statistics',
        type: 'report'
      },
      {
        label: '道路使用性能评价报表',
        parentType: 'statistics',
        type: 'road-usage',
        sub: [
          {
            label: '行驶质量评价',
            type: 'rqi'
          },
          {
            label: '损坏状况评价',
            type: 'pci'
          },
          {
            label: '交通状况评价',
            type: 'aadt'
          }
        ],
        disabled: false,
        tip: '需首先在「数据导入」中完成「数据库」的动态数据导入，并在「分析评价」中完成「数据库」的PQI、RQI、ESAL的计算后方可查看'
      },
      {
        label: '检测道路建议大中修项目报表',
        parentType: 'statistics',
        type: 'road-advice',
        sub: [
          {
            label: '建议大中修项目统计',
            type: 'maintain'
          },
          {
            label: '建议大中修项目明细',
            type: 'maintain2'
          }
        ],
        disabled: false,
        tip: '需首先在「分析评价」中完成「数据库」的决策树方案计算后方可查看'
      },
      {
        label: '预防性养护项目建议报表',
        parentType: 'statistics',
        type: 'maintain',
        disabledData: [4],
        disabled: false,
        tip: '需首先在「分析评价」中完成「数据库」的路面行驶质量指数RQI的计算后方可查看'
      },
      {
        label: '中长期养护规划报表',
        parentType: 'statistics',
        type: 'road-plan',
        sub: [
          {
            label: '中长期养护规划项目统计',
            type: 'performance'
          },
          {
            label: '道路使用性能变化',
            type: 'performance2'
          }
        ],
        disabledData: [4],
        disabled: false,
        tip: '需首先在「分析评价」中完成「数据库」的各项中长期养护规划指标的计算后方可查看'
      },
      {
        label: '各区道路总体情况报表',
        parentType: 'statistics',
        type: 'compare-district',
        sub: []
      }
    ];
    return sidebars;
  })())
  // 地区报表侧边栏
  .value('districtSidebars', (function () {
    var sidebars = [
      {
        label: '概况',
        parentType: 'statistics',
        type: 'district'
      },
      {
        label: '道路使用性能评价报表',
        parentType: 'statistics',
        type: 'road-usage',
        sub: [
          {
            label: '行驶质量评价',
            type: 'rqi'
          },
          {
            label: '损坏状况评价',
            type: 'pci'
          },
          {
            label: '交通状况评价',
            type: 'aadt'
          }
        ],
        disabled: false,
        tip: '需首先在「数据导入」中完成「数据库」的动态数据导入，并在「分析评价」中完成「数据库」的PQI、RQI、ESAL的计算后方可查看'
      },
      {
        label: '检测道路建议大中修项目报表',
        parentType: 'statistics',
        type: 'road-advice',
        sub: [
          {
            label: '维修需求统计分析',
            type: 'maintain'
          },
          {
            label: '建议大中修项目统计',
            type: 'maintain2'
          }
        ],
        disabled: false,
        tip: '需首先在「分析评价」中完成「数据库」的决策树方案计算后方可查看'
      },
      {
        label: '预防性养护项目建议报表',
        parentType: 'statistics',
        type: 'maintain',
        disabledData: [4],
        disabled: false,
        tip: '需首先在「分析评价」中完成「数据库」的路面行驶质量指数RQI的计算后方可查看'
      },
      {
        label: '中长期养护规划报表',
        parentType: 'statistics',
        type: 'road-plan',
        sub: [
          {
            label: '中长期养护规划统计分析',
            type: 'performance'
          },
          {
            label: '道路使用性能变化',
            type: 'performance2'
          },
          {
            label: '中长期养护规划项目统计',
            type: 'performance3'
          }
        ],
        disabledData: [4],
        disabled: false,
        tip: '需首先在「分析评价」中完成「数据库」的各项中长期养护规划指标的计算后方可查看'
      },
      {
        label: '本年度检测道路动态数据一览表',
        parentType: 'statistics',
        type: 'dynamic'
      },
      {
        label: '路段综合分析报表',
        parentType: 'statistics',
        type: 'summaryList',
        disabledData: [14],
        disabled: false,
        tip: '需首先在「分析评价」中完成「数据库」的路段综合分析后方可查看'
      }
    ];
    return sidebars;
  })())
  .directive("reportSidebar", ['systemIF', 'Notify', '$route', 'reportSidebars', '$rootScope', '$http', '$routeParams', 'reportIF', '$location', function (systemIF, Notify, $route, sidebars, $rootScope, $http, $routeParams, reportIF, $location) {
    return {
      restrict: "E",
      link: function ($scope, element, attrs) {
        $scope.sidebars = sidebars;
        $scope.activeIndex = -1;
        $scope.activeSonIndex = -1;
        $scope.moduleName = $route.current.moduleName;
        $scope.tableName = $route.current.tableName;
        $scope.district = $routeParams.district; // 地区名字
        var districtType = $routeParams.districtType == 2 ? 1 : 0; // 1市区 2郊区

        // 检测数据
        var watch = $rootScope.$watch('globalYear', function (data) {
          reportIF.getFinishTask().success(function (data) {
            if (data.status == 0) {
              $scope.sidebars = sidebars.map(function (v, k) {
                // 有标示的情况下面
                if (v.disabledData) {
                  v.disabled = !window.isContained(data.data[$rootScope.globalYear], v.disabledData) || false;
                  // 当前栏目是否是需要无数据提示
                  if (k == $scope.activeIndex) {
                    if (v.disabled == true) {
                      $rootScope.showPageNoDataTip = true;
                    } else {
                      $rootScope.showPageNoDataTip = false;
                    }
                  }
                }
                return v;
              });
            }
          });
        });
        var watch2 = $rootScope.$watch('globalDistrictList', function (data) {
          var list = data || [];
          var newSidebarData = [];
          angular.forEach(list, function (v, k) {
            var obj = {
              label: v.label,
            }
            newSidebarData.push(obj);
          });
          $scope.sidebars[$scope.sidebars.length - 1].last = true;
          $scope.sidebars[$scope.sidebars.length - 1].sub = newSidebarData;
        });
        $rootScope.globalWatch.watchReportSidebar = watch;
        $rootScope.globalWatch.watchReportSidebar2 = watch2;
        $scope.$on("$destroy", function () {
          watch();
          watch2();
        });

        var getActiveItem = sidebars.filter(function (v, k) {
          v.index = k;
          return v.type == $route.current.moduleName;
        });
        $scope.activeSonIndex = districtType;

        if (getActiveItem.length) {
          $scope.activeIndex = getActiveItem[0].index;
        }
        // 二级菜单
        $scope.toggleSub = function (index) {
          if ($scope.activeIndex === index) {
            $scope.activeIndex = -1;
            return;
          }
          $scope.activeIndex = index;
        };
        // 三级菜单
        $scope.toggleSon = function (index) {
          if (index == $scope.activeSonIndex) {
            $scope.activeSonIndex = -1;
          } else {
            $scope.activeSonIndex = index;
          }
        }
      },
      templateUrl: sidebar
    }
  }])
  .directive("districtSidebar", ['systemIF', 'Notify', '$route', 'districtSidebars', '$routeParams', '$rootScope', 'reportIF', function (systemIF, Notify, $route, sidebars, $routeParams, $rootScope, reportIF) {
    return {
      restrict: "E",
      link: function ($scope, element, attrs) {
        $scope.sidebars = sidebars;
        $scope.mTabItemActive = 0;
        $scope.moduleName = $route.current.moduleName;
        $scope.tableName = $route.current.tableName;
        $scope.districtType = $routeParams.districtType || $rootScope.districtType;
        $scope.district = $routeParams.district || $rootScope.globalUserinfo.currentUser.district;

        // 检测数据
        var watch = $rootScope.$watch('globalYear', function (data) {
          reportIF.getFinishTask().success(function (data) {
            if (data.status == 0) {
              $scope.sidebars = sidebars.map(function (v, k) {
                // 有标示的情况下面
                if (v.disabledData) {
                  v.disabled = !window.isContained(data.data[$rootScope.globalYear], v.disabledData) || false;

                  // 当前栏目是否是需要无数据提示
                  if (k == $scope.activeIndex) {
                    if (v.disabled == true) {
                      $rootScope.showPageNoDataTip = true;
                    } else {
                      $rootScope.showPageNoDataTip = false;
                    }
                  }
                }
                return v;
              });
            }
          });
        });
        $rootScope.globalWatch.watchDistrictSidebar = watch;
        $scope.$on("$destroy", function () {
          watch();
        });

        var getActiveItem = sidebars.filter(function (v, k) {
          v.index = k;
          return v.type == $route.current.moduleName;
        });

        if (getActiveItem.length) {
          $scope.mTabItemActive = getActiveItem[0].index;
        }

        $scope.updateMtabItemActive = function (index) {
          console.log(angular.element('.nav__link.active'));
          if ($scope.mTabItemActive === index) {
            $scope.mTabItemActive = 0;
            return;
          }
          $scope.mTabItemActive = index;

        };
      },
      templateUrl: sidebarDistrict
    }
  }])
  .service('reportIF', ['$http', 'commonData', 'Tool', function ($http, commonData, Tool) {
    var reportIF = {
      pageInfo: function () {
        var params = {
          signature: window.getSignature('')
        }
        return $http.get('/report/pageinfo', {
          params: params
        });
      },
      summaryPageinfo: function (params) {
        return $http.get('/report/summary/pageinfo', {
          params: params
        });
      },
      getSummaryDataByGrade: function (params) {
        return $http.get('/report/summary/getDataByGrade', {
          params: params
        });
      },
      getSummaryDataByDistrict: function (params) {
        return $http.get('/report/summary/getDataByDistrict', {
          params: params
        });
      },
      getDataByGrade: function (params, pType, type) {
        return $http.get('/report/' + pType + '/' + type + '/getDataByGrade', {
          params: params
        });
      },
      getDataByDistrict: function (params, pType, type) {
        return $http.get('/report/' + pType + '/' + type + '/getDataByDistrict', {
          params: params
        });
      },
      getDataByDistrictInDistrict: function (params, pType, type) {
        return $http.get('/report/district/' + pType + '/' + type + '/getDataByDistrict', {
          params: params
        });
      },
      getDataByGradeInDistrict: function (params, pType, type) {
        return $http.get('/report/district/' + pType + '/' + type + '/getDataByGrade', {
          params: params
        });
      },
      aadtGetlist: function (params) {
        return $http.get('/report/road-usage/aadt/getlist', {
          params: params
        });
      },
      aadtGetlistInDistrict: function (params) {
        return $http.get('/report/district/road-usage/aadt/getlist', {
          params: params
        });
      },
      getMaintainList: function (params) {
        return $http.get('/report/road-advice/maintain/getlist', {
          params: params
        });
      },
      getMaintainListInDistrict: function (params) {
        return $http.get('/report/district/road-advice/maintain/getlist', {
          params: params
        });
      },
      getAdvicelist: function (params) {
        return $http.get('/report/road-advice/maintain/advicelist', {
          params: params
        });
      },
      getAdvicelistInDistrict: function (params) {
        return $http.get('/report/district/road-advice/maintain/advicelist', {
          params: params
        });
      },
      getPerformanceUsage: function (params) {
        return $http.get('/report/road-plan/performance/usage', {
          params: params
        });
      },
      getPerformanceUsageInDistrict: function (params) {
        return $http.get('/report/district/road-plan/performance/usage', {
          params: params
        });
      },
      districtCompare: function (params) {
        return $http.get('/report/district/summary/compare', {
          params: params
        });
      },
      summaryGetlist: function (params) {
        return $http.get('/report/district/summary/getlist', {
          params: params
        });
      },
      getPCILess75List: function (params) {
        return $http.get('/report/district/road-plan/performance/getPCILess75List', {
          params: params
        });
      },
      getDynamicList: function (params) {
        return $http.get('/report/district/road-plan/performance/getDynamicList', {
          params: params
        });
      },
      summaryDetail: function (params) {
        return $http.get('/report/summary/detail', {
          params: params
        });
      },
      summaryList: function (params) {
        return $http.get('/report/summary/getRoadExcelList', {
            params: params
        });
    },
      getFinishTask: function () {
        var params = {
          signature: window.getSignature('')
        }
        return $http.get('/task/getFinishTask', {
          params: params
        });
      },
      taskList: function (params) {
        return $http.get('/task/getlist', {
          params: params
        });
      }
    };
    return angular.extend({}, commonData, reportIF);
  }])
  ;





