/**
 * Created by Jessie on 16/11/16.
 */

'use strict';

var pageUrl = require('./analysis.html');
var timer = null; // 定时器
angular
  .module('app.analysis', [])
  .config(['$routeProvider', '$locationProvider', '$httpProvider', function ($routeProvider, $locationProvider, $httpProvider) {
    $routeProvider
      .when('/analysis', {
        templateUrl: pageUrl,
        controller: 'analysisController',
        state: 'analysis'
      })
      ;
  }])
  .run(['$rootScope', function ($rootScope) {
    // 路由跳转之前
    $rootScope.$on('$routeChangeStart', function (ev, to) {
      clearInterval(timer);
    });
  }])
  // 主活动详情
  .controller('analysisController', ['$scope', '$routeParams', 'analysisIF', 'Notify', 'Alert', '$rootScope', '$location', '$route', function ($scope, $routeParams, analysisIF, Notify, Alert, $rootScope, $location, $route) {
    var optionList = $scope.optionList = {};
    $scope.optionListLength = 0;
    var form = $scope.form = {
      order: 'asc',
      offset: 1,
      count: 10
    };
    $scope.taskList = []; // 任务数据
    var timerTime = 0; // 20分钟之后停止循环
    $scope.stage4Url = '';

    function listOptionList() {
      var params = {
        year: $scope.globalYear
      }
      analysisIF.listOptionList(params).success(function (data) {
        if (data.status == 0) {
          optionList = $scope.optionList = data.data;
          var arr = Object.keys(data.data);
          $scope.optionListLength = arr.length;
        }
      });
    }

    function list() {
      analysisIF.taskList($scope.form).success(function (data) {
        if (data.status == 0) {
          $scope.taskList = data.data.list || [];
          form.total = data.data.total;
          $scope.btnStatus = data.data.finish || {};
        }
      });
    }

    $scope.pageChanged = function () {
      list();
    };

    $scope.doTask = function (key) {
      var params = {
        name: $rootScope.globalYear + optionList[key].name,
        year: $rootScope.globalYear,
        workerType: key
      }
      analysisIF.doTask(params).success(function (data) {
        if (data.status == 0) {
          Alert.alert('正在计算' + optionList[key].name + '，请稍等...');
          optionList[key].status = 1;
          list();
        } else {
          Notify.alert({
            message: data.msg,
            hideCancel: true
          });
        }
      });
    }

    $scope.goFirstUrl = function () {
      if ($rootScope.globalUserinfo.currentUser.user_type == 3) {
        $location.path('/dsearch');
      } else {
        $location.path('/data/search');
      }
    }

    $scope.goUrl = function (key) {
      const districtList = $rootScope.globalDistrictList;
      console.log(districtList, 'districtList');
      switch (key) {
        case 4:
          if (!districtList.length) {
            Alert.alert('暂无分区报表数据，无法跳转！', 'danger');
          } else {
            $location.path('/statistics/summaryList/' + districtList[0].label);
          }
          break;
        case 3:
          $location.path('/data/search/1');
          break;
        case 2:
          $location.path('/statistics/road-advice/maintain');
          break;
        case 1:
          $location.path('/statistics/road-plan/performance');
          break;
        default:
          break;
      }
    }

    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      listOptionList();
    });
    $rootScope.globalWatch.watchListOptionList = watch;
    $scope.$on("$destroy", function () {
      watch();
    });
    list();
    timer = setInterval(function () {
      list();
      timerTime += 10000;
      if (timerTime == 600000) {
        clearInterval(timer);
      }
    }, 10000);
  }])
  .service('analysisIF', ['$http', 'commonData', 'Tool', function ($http, commonData, Tool) {
    var analysisIF = {
      listOptionList: function (params) {
        return $http.get('/task/oplist', {
          params: params
        });
      },
      taskList: function (params) {
        return $http.get('/task/getlist', {
          params: params
        });
      },
      doTask: function (params) {
        return $http.post('/task/add', params);
      }
    };
    return angular.extend({}, commonData, analysisIF);
  }])
  ;





