/**
 * Created by Jessie on 16/11/16.
 */

'use strict';

var pageUrl = require('./user.html');
var pwdModal = require('./updatePwd.html');
angular
    .module('app.user', [])
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/user', {
                templateUrl: pageUrl,
                controller: 'userController',
                state: 'user'
            })
        ;
    }])
    // 主活动详情
    .controller('userController', ['$scope', '$routeParams', 'userIF', 'Notify', 'pwdModal', 'Alert', function($scope, $routeParams, userIF, Notify, pwdModal, Alert) {
        var form = $scope.form = {
            offset: 1,
            count: 10,
            username: '',
            total: 0
        };
        $scope.list = [];
        var formConf = $scope.formConf = {};
        $scope.org_type = '1';
        var user = $scope.user = {};
        $scope.isUpdate = false;
        var savePassword = '';

        function list() {
            userIF.list(form).success(function(data) {
                if (data.status == 0 && data.data && data.data.list) {
                    $scope.list = data.data.list;
                    form.total = data.data.total;
                }
            });
        }
        function listConf() {
            userIF.listConf().success(function(data) {
                $scope.formConf = data.data;
            });
        }

        $scope.pageChanged = function() {
            list();
        };
        $scope.search = function() {
            list();
        };

        $scope.reset = function() {
            form.username = '';
            form.offset = 1;
            list();
        };

        $scope.save = function() {
            if ($scope.isUpdate) {
                if (user.user_type == 1) {
                    user.district = '';
                }
                var newObj = {
                    user_id: user.user_id,
                    username: window.getAES(user.username),
                    password: window.getAES(user.password),
                    user_type: user.user_type,
                    district: user.district
                };
                userIF.update(newObj).success(function(data) {
                    if (data.status == 0) {
                        $scope.isUpdate = false;
                        user = $scope.user = {};
                        list();
                        Alert.alert('操作成功！');
                    }
                });
            } else {
                if (user.user_type == 1) {
                    user.district = '';
                }
                user.username = window.getAES(user.username);
                user.password = window.getAES(user.password);
                userIF.save(user).success(function(data) {
                    if (data.status == 0) {
                        list();
                        user = $scope.user = {};
                        Alert.alert('操作成功！');
                    }
                });
            }
        };
        $scope.toEdit = function(item) {
            $scope.isUpdate = true;
            user = $scope.user = angular.extend({}, item);
        };
        $scope.cancelEdit = function() {
            $scope.isUpdate = false;
            user = $scope.user = {}
        };
        $scope.delete = function(id) {
            var obj = {
                userId: id
            };
            Notify.alert('确定删除?').result.then(function () {
                userIF.delete(obj).success(function(data) {
                    if (data.status == 0) {
                        Alert.alert('删除成功');
                        list();
                    }
                });
            });

        };
        $scope.openRight = function(item) {
            var obj = {
                user_id: item.user_id
            };
            if (item.status == 0) {
                userIF.closeRight(obj).success(function(data) {
                    if (data.status == 0) {
                        item.status = 1;
                    }
                });
            } else {
                userIF.openRight(obj).success(function(data) {
                    if (data.status == 0) {
                        item.status = 0;
                    }
                });
            }
        };
        $scope.updatePassword = function (item) {
            pwdModal.exec(item.user_id).result.then(function(result) {
                Alert.alert(result);
            });
        };
        list();
        listConf();
    }])
    .controller('pwdModalController', ['$scope', '$uibModalInstance', 'userIF', 'item', function ($scope, $uibModalInstance, userIF, item) {
        $scope.repeatPwd = '';
        var form = $scope.form = {
            user_id: item,
            old_pwd: '',
            new_pwd: ''
        };
        $scope.savePwd = function () {
            if ($scope.repeatPwd != form.new_pwd) {
                return;
            }
            var newObj = {
                user_id: item,
                old_pwd: window.getAES(form.old_pwd),
                new_pwd: window.getAES(form.new_pwd)
            }
            userIF.updatePassword(newObj).success(function(data) {
                if (data.status == 0) {
                    $uibModalInstance.close('操作成功！');
                } else {
                    $uibModalInstance.close(data.message);
                }
            });
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .service('pwdModal', ['$uibModal', function ($uibModal) {
        return {
            exec: function (item) {
                return $uibModal.open({
                    backdrop: 'static',
                    templateUrl: pwdModal,
                    controller: 'pwdModalController',
                    resolve: {
                        item: function () {
                            if (item) {
                                return item;
                            }
                        }
                    }
                });
            }
        }
    }])
    .filter('getUserType', function() { //可以注入依赖
        return function(str, data) {
            var result = [];
            if (str && data) {
                result = data.filter(function(v, k) {
                    return v.id == str;
                });
            }
            return result[0] ? result[0].name : '';
        }
    })
    .service('userIF', ['$http', 'commonData', 'Tool', function($http, commonData, Tool) {
        var userIF = {
            list: function (params) {
                return $http.get('/user/api/getlist', {
                    params: params
                });
            },
            listConf: function () {
                var params = {
                    signature: window.getSignature('')
                }
                return $http.get('/user/pageinfo/get', {
                    params: params
                });
            },
            save: function (params) {
                return $http.post('/user/api/add', params);
            },
            update: function (params) {
                return $http.post('/user/api/edit', params);
            },
            delete: function (params) {
                return $http.post('/user/api/delete', params);
            },
            openRight: function (params) {
                return $http.post('/user/api/resume', params);
            },
            closeRight: function (params) {
                return $http.post('/user/api/suspend', params);
            },
            updatePassword: function (params) {
                return $http.post('/user/api/editPasswd', params);
            }
        };
        return angular.extend({}, commonData, userIF);
    }])
;





