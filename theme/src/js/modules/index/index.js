/**
 * Created by Jessie on 16/11/16.
 */

'use strict';

var pageUrl = require('./index.html');

angular
    .module('app.index', [])
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/index', {
                templateUrl: pageUrl,
                controller: 'indexController',
                state: 'index'
            });
    }])
    // 主活动详情
    .controller('indexController', ['$scope', '$routeParams', 'Notify', 'Alert', '$rootScope', function($scope, $routeParams, indexIF, Notify, Alert, mapType, location, $rootScope) {
        var map = new BMap.Map("container");
        // 创建地图实例
        var point = new BMap.Point(123.47327,41.684921);
        // 创建点坐标
        map.centerAndZoom(point, 12);
        map.addControl(new BMap.NavigationControl());
        map.addControl(new BMap.ScaleControl());    
        map.addControl(new BMap.OverviewMapControl());    
    }]);