/**
 * Created by Jessie on 16/11/16.
 */

'use strict';

var pageUrl = require('./pwd.html');
angular
    .module('app.pwd', [])
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/pwd', {
                templateUrl: pageUrl,
                controller: 'pwdController',
                state: 'pwd'
            })
        ;
    }])
    // 主活动详情
    .controller('pwdController', ['$scope', '$routeParams', 'pwdIF', 'Notify', 'Alert', '$rootScope', function($scope, $routeParams, pwdIF, Notify, Alert ,$rootScope) {
        $scope.repeatPwd = '';
        var form = $scope.form = {
            user_id: $rootScope.globalUserinfo.currentUser.user_id,
            old_pwd: '',
            new_pwd: ''
        };
        $scope.savePwd = function () {
            if ($scope.repeatPwd != form.new_pwd) {
                return;
            }
            var newObj = {
                user_id: $rootScope.globalUserinfo.currentUser.user_id,
                old_pwd: window.getAES(form.old_pwd),
                new_pwd: window.getAES(form.new_pwd)
            }
            pwdIF.updatePassword(newObj).success(function(data) {
                if (data.status == 0) {
                    Notify.alert({
                        message: '密码修改成功！',
                        hideCancel: true
                    }).result.then(function () {
                        window.location.href = '/admin/logout';
                    });
                } else {
                    Alert.alert(data.msg);
                }
            });
        };
    }])
    .service('pwdIF', ['$http', 'commonData', 'Tool', function($http, commonData, Tool) {
        var pwdIF = {
            updatePassword: function (params) {
                return $http.post('/user/api/editPasswd', params);
            }
        };
        return angular.extend({}, commonData, pwdIF);
    }])
;





