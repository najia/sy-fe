/**
 * Created by Jessie on 16/11/16.
 */

'use strict';

var pageUrl = require('./delete.html');
angular
    .module('app.delete', [])
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/data/delete', {
                templateUrl: pageUrl,
                controller: 'deleteController',
                state: 'delete'
            });
    }])
    // 主活动详情
    .controller('deleteController', ['$scope', '$routeParams', 'deleteIF', 'Notify', 'Alert', function($scope, $routeParams, deleteIF, Notify, Alert) {
        $scope.year = '';
        var deletedYearList = $scope.deletedYearList = [];

        if (window.T && window.T.expire) {
            const expire = window.T.expire;
            for (var key in expire) {
                if (expire[key].can_edit) {
                    deletedYearList.push(key)
                }
            }
        }
        $scope.toDelete = function() {
            Notify.alert('即将删除【'+ $scope.year +'】数据库，是否确认？').result.then(function () {
                var params = {
                    year: $scope.year
                }
                deleteIF.delete(params).success(function(data) {
                    if (data.status == 0) {
                        Alert.alert('操作成功！');

                        setTimeout(function() {
                            window.location.reload();
                        }, 2500);
                    }
                }); 
            }, function() {
                // 点击否
            });
        };
 
    }])
    .service('deleteIF', ['$http', 'commonData', 'Tool', function($http, commonData, Tool) {
        var deleteIF = {
            delete: function(params) {
                return $http.post('/import/api/delete', params);
            },
        };
        return angular.extend({}, commonData, deleteIF);
    }]);