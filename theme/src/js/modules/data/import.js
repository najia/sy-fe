/**
 * Created by Jessie on 16/11/16.
 */

'use strict';

var pageUrl = require('./import.html');
var statusModal = require('./statusModal.html');
var timer = null; // 定时器
angular
    .module('app.import', [])
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/data/import', {
                templateUrl: pageUrl,
                controller: 'importController',
                state: 'import'
            });
    }])
    .run(['$rootScope',  function($rootScope) {
        // 路由跳转之前
        $rootScope.$on('$routeChangeStart', function(ev, to) {
            clearInterval(timer);
        });
    }])
    // 主活动详情
    .controller('importController', ['$scope', '$routeParams', 'importIF', 'Notify', 'Alert', 'FileUploader', '$rootScope', 'statusModal', function($scope, $routeParams, importIF, Notify, Alert, FileUploader, $rootScope, statusModal) {
        var form = $scope.form = {
            year: new Date().getUTCFullYear(),
            filename: ''
        }
        var formTask = $scope.formTask = {
            order: 'asc',
            offset: 1,
            count: 10,
            type: 15
        };
        $scope.taskList = []; // 任务数据
        var timerTime = 0; // 20分钟之后停止循环
        $rootScope.uploading = false;
        var file = document.getElementById('upload');

        // 统计年份
        $scope.importYear = [];
        var currentYear = new Date().getUTCFullYear();
        for (var i = 2014; i <= currentYear; i++) {
            $scope.importYear.push(i);
        }
        $scope.importYear.reverse();

        // 上传文件
        var uploader = $scope.uploader = new FileUploader({
            url: '/upload?fileName=file',
            alias: 'file',
            formData: [{
                type: 4
            }],
            queueLimit: 3,
            autoUpload: false,
            // removeAfterUpload: true,
            filters: [{
                name: 'fileFilter',
                fn: function(item, options) {
                    var type = '|' + item.name.slice(item.name.lastIndexOf('.') + 1) + '|';
                    var ext = '|xls|xlsx|';
                    var isFileType = ext.indexOf(type) !== -1 && item.size > 0;
                    if (!isFileType) {
                        Alert.alert('上传失败，只能上传Excel文件！', 'danger');
                    }
                    form.filename = '';
                    return isFileType;
                }
            }],
            onAfterAddingFile: function(value) {

            },
            onSuccessItem: function(item, response, status, headers) {
                $rootScope.uploading = false;
                if (response.status == 0) {
                    if (!response.data) {
                        file.value = '';
                        Alert.alert('上传失败！', 'danger');
                        return;
                    }
                    // 假设切换到其他页面了，终止后面的保存操作
                    if ($rootScope.stopUploading) {
                        return;
                    }
                    $scope.uploaded = true;
                    form.filename = response.data;
                } else {
                    Alert.alert(response.msg || '上传失败！', 'danger');
                }
            },
            onCompleteAll: function() {
                uploader.clearQueue();
            }
        });

        function list() {
            importIF.taskList($scope.formTask).success(function(data) {
                if (data.status == 0) {
                    $scope.taskList = data.data.list || [];
                    formTask.total = data.data.total;
                }
            });
        }

        $scope.pageChanged = function() {
            list();
        };

        $scope.toSave = function() {
            if (!file.value || !form.year) {
                Alert.alert('请先补充完上传动态数据所需的年份信息和数据文件所在路径信息！', 'danger');
                return;
            }
            Notify.alert('是否立即上传'+ form.year +'年动态数据').result.then(function () {
                $rootScope.uploading = true;
                // 上传文件
                uploader.uploadAll();
                statusModal.exec(uploader).result.then(function() {
                    // 保存数据
                    save();
                });
            }, function() {
                // 点击否
            });
        }

        function save() {
            importIF.save(form).success(function(data) {
                if (data.status == 0) {
                    file.value = '';
                    form.filename = '';
                    
                    list();
                    Alert.alert('保存成功！');

                    setTimeout(function() {
                        window.location.reload();
                    }, 2500);
                }
            });
        }

        list();
        // $rootScope.globalWatch.watchImport = watch;
        timer = setInterval(function() {
            list();
            timerTime += 10000;
            if (timerTime == 600000) {
                clearInterval(timer);
            }
        }, 10000);
 
    }])
    .controller('statusModalController', ['$scope', '$uibModalInstance', 'userIF', 'item', function ($scope, $uibModalInstance, userIF, item) {
        var uploader = $scope.uploader = item;
        uploader.onCompleteAll = function() {
            if(uploader.progress == 100) {
                // 关闭窗口
                $uibModalInstance.close();
            }
        }
        
    }])
    .service('statusModal', ['$uibModal', function ($uibModal) {
        return {
            exec: function (item) {
                return $uibModal.open({
                    backdrop: 'static',
                    templateUrl: statusModal,
                    controller: 'statusModalController',
                    resolve: {
                        item: function () {
                            if (item) {
                                return item;
                            }
                        }
                    }
                });
            }
        }
    }])
    .service('importIF', ['$http', 'commonData', 'Tool', function($http, commonData, Tool) {
        var importIF = {
            save: function(params) {
                return $http.post('/import/api/importDynamic', params);
            },
            list: function(params) {
                return $http.get('/road_data/api/getlist', {
                    params: params
                });
            },
            taskList: function(params) {
                return $http.get('/task/getlist', {
                    params: params
                });
            },
            hasData: function(year) {
                return $http.get('/import/api/hasData?year=' + year);
            },
            getUpdateDataTime: function(year) {
                return $http.get('/task/getlist?year='+ year +'&type=16&name=下载报表');
            },            
            updateData: function(year) {
                return $http.get('/task/add?year='+ year +'&workerType=16&name=导入静态数据');
            }
        };
        return angular.extend({}, commonData, importIF);
    }]);