/**
 * Created by Jessie on 16/11/16.
 */

'use strict';

var pageUrl = require('./data.html');
angular
  .module('app.data', [])
  .config(['$routeProvider', '$locationProvider', '$httpProvider', function ($routeProvider, $locationProvider, $httpProvider) {
    $routeProvider
      .when('/data/search', {
        templateUrl: pageUrl,
        controller: 'dataController',
        state: 'search'
      })
      .when('/dsearch', {
        templateUrl: pageUrl,
        controller: 'dataController',
        state: 'dsearch'
      })
      .when('/data/search/:id', {
        templateUrl: pageUrl,
        controller: 'dataController',
        state: 'search'
      })
      .when('/dsearch/:id', {
        templateUrl: pageUrl,
        controller: 'dataController',
        state: 'dsearch'
      })
      ;
  }])
  // 主活动详情
  .controller('dataController', ['$scope', '$routeParams', 'dataIF', 'Notify', 'Alert', '$rootScope', 'Tool', function ($scope, $routeParams, dataIF, Notify, Alert, $rootScope, Tool) {
    $scope.datepicker = {
      open: function ($event, type) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.datepicker[type + 'Opened'] = true;
      }
    };
    $scope.buildDate = {
      start: {
        maxDate: '',
        change: function () {
          $scope.buildDate.end.minDate = $scope.form.stBuildDate;
        }
      },
      end: {
        minDate: '',
        change: function () {
          $scope.buildDate.start.maxDate = $scope.form.etBuildDate;
        }
      }
    }
    $scope.lastBuildDate = {
      start: {
        maxDate: '',
        change: function () {
          $scope.lastBuildDate.end.minDate = $scope.form.stLastBuildDate;
        }
      },
      end: {
        minDate: '',
        change: function () {
          $scope.lastBuildDate.start.maxDate = $scope.form.etLastBuildDate;
        }
      }
    }
    var form = $scope.form = {
      year: $rootScope.globalYear,
      order: 'asc',
      offset: 1,
      count: 10
    };
    var pageConf = $scope.pageConf = {};
    var getFields = {}; // 只是当成数据存储
    var getStaticFields = {};
    var getDynamicFields = {};
    var getDecisionFields = {};
    $scope.fields = []; // 表头数据
    $scope.staticFields = {}; // 静态字段， 存储页面点击的数据
    $scope.dynamicFields = {}; // 动态字段， 存储页面点击的数据
    $scope.decisionFields = {}; // 决策信息， 存储页面点击的数据
    $scope.list = []; // 列表数据
    $scope.styleW = {};
    $scope.checkStatic = '1'; // 静态数据checkbox
    $scope.checkDynamic = '1'; // 动态数据checkbox
    $scope.checkDecision = '1'; // 决策信息checkbox
    $scope.isObvious = false; // 背景是否绿色

    function initCheckItem() {
      for (var key in getStaticFields) {
        $scope.staticFields[key] = true;
      }
      for (var key in getDynamicFields) {
        $scope.dynamicFields[key] = true;
      }

      for (var key in getDecisionFields) {
        $scope.decisionFields[key] = true;
      }
    }
    function getPageConf() {
      dataIF.pageConf($rootScope.globalYear).success(function (data) {
        if (data.status == 0 && data) {
          pageConf = $scope.pageConf = data.data || {};
          $scope.fields = angular.extend({}, pageConf.static, pageConf.dynamic);
          getStaticFields = pageConf.static;
          getDynamicFields = pageConf.dynamic;
          getDecisionFields = pageConf.decision;
          getFields = angular.extend({}, pageConf.static, pageConf.dynamic, pageConf.decision);
          // 将checkbox的值进行初始化
          initCheckItem();
          var urlParams = $routeParams.id;
          // 有参数的时候直接跳转结果
          if (urlParams) {
            $scope.isObvious = true;
            $scope.search();
          }
        }
      });
    }

    function list() {
      $scope.isSearch = true;
      document.documentElement.scrollTop = 0;
      dataIF.list($scope.form).success(function (data) {
        if (data.status == 0) {
          $scope.list = data.data.list || [];
          form.total = data.data.total;
          if ($scope.list.length) {
            var width = Object.keys($scope.fields).length * 200;
            var wrapWidth = document.getElementById('table-wrap').clientWidth;

            if (width > wrapWidth) {
              $scope.styleW.width = Object.keys($scope.fields).length * 120 + 'px';
            } else {
              $scope.styleW.width = {};
            }
          } else {
            $scope.styleW.width = '100%';
          }
        } else {
          $scope.list = [];
          form.total = 0;
          $scope.styleW.width = '100%';
        }
      });
    }

    function urlEncode(param, key, encode) {
      if (param == null) return '';
      var paramStr = '';
      var t = typeof (param);
      if (t == 'string' || t == 'number' || t == 'boolean') {
        paramStr += '&' + key + '=' + ((encode == null || encode) ? encodeURIComponent(param) : param);
      } else {
        for (var i in param) {
          var k = key == null ? i : key;
          // console.log(k);
          paramStr += urlEncode(param[i], k, encode);
        }
      }
      return paramStr;
    };

    $scope.download = function () {
      var url = '/road_data/api/download?' + urlEncode($scope.form).substring(1, urlEncode($scope.form).length - 1);
      window.location.href = url;
    };

    $scope.search = function () {
      var staticField = [];
      var dynamicField = [];
      var decisionField = [];
      for (var key in $scope.staticFields) {
        if ($scope.staticFields[key]) {
          staticField.push(key);
        }
      }
      for (var key in $scope.dynamicFields) {
        if ($scope.dynamicFields[key]) {
          dynamicField.push(key);
        }
      }
      
      for (var key in $scope.decisionFields) {
        if ($scope.decisionFields[key]) {
          decisionField.push(key);
        }
      }

      if (!staticField.length && !dynamicField.length && !decisionField.length) {
        Alert.alert('自定义查询字段不能为空！', 'danger');
        return;
      }
      form['staticFields[]'] = staticField;
      form['dynamicFields[]'] = dynamicField;
      form['decisionFields[]'] = decisionField;
      var newFields = staticField.concat(dynamicField).concat(decisionField);
      // 重组表头
      if (newFields.length) {
        var getNewFields = {};
        for (var key in getFields) {
          for (var i = 0; i < newFields.length; i++) {
            if (key == newFields[i]) {
              getNewFields[key] = getFields[key];
            }
          }
        }
        $scope.fields = getNewFields;
      } else {
        $scope.fields = getFields;
      }
      form.offset = 1;
      list();
    };

    $scope.sort = function (key) {
      form.order = form.order == 'asc' ? 'desc' : 'asc';
      form.orderBy = key;
      list();
    };

    $scope.toCheckStatic = function () {
      if ($scope.checkStatic == 1) {
        for (var key in $scope.staticFields) {
          $scope.staticFields[key] = true;
        }
      } else {
        for (var key in $scope.staticFields) {
          if ($scope.staticFields[key]) {
            $scope.staticFields[key] = false;
          } else {
            $scope.staticFields[key] = true;
          }
        }
      }
    };

    $scope.toCheckDynamic = function () {
      console.log($scope.checkDynamic, '$scope.checkDynamic ');
      if ($scope.checkDynamic == 1) {
        for (var key in $scope.dynamicFields) {
          $scope.dynamicFields[key] = true;
        }
      } else {
        for (var key in $scope.dynamicFields) {
          if ($scope.dynamicFields[key]) {
            $scope.dynamicFields[key] = false;
          } else {
            $scope.dynamicFields[key] = true;
          }
        }
      }
    };

    $scope.toCheckDecision = function () {
      console.log($scope.checkDecision, '$scope.checkDecision');
      if ($scope.checkDecision == 1) {
        for (var key in $scope.decisionFields) {
          $scope.decisionFields[key] = true;
        }
      } else {
        for (var key in $scope.decisionFields) {
          if ($scope.decisionFields[key]) {
            $scope.decisionFields[key] = false;
          } else {
            $scope.decisionFields[key] = true;
          }
        }
      }
    };

    $scope.backSearch = function () {
      $scope.isSearch = !$scope.isSearch;
      document.documentElement.scrollTop = 0;
    };

    $scope.pageChanged = function () {
      list();
    };

    // 检测数据
    var watch = $rootScope.$watch('globalYear', function (data) {
      form.year = $rootScope.globalYear;
      $scope.isSearch = false; // 是否是点击了查询
      $scope.list = [];
      form.total = 0;
      $scope.styleW.width = '100%';

      getPageConf();
    });
    $rootScope.globalWatch.watchSearch = watch;
    $scope.$on("$destroy", function () {
      watch();
    })

  }])
  .service('dataIF', ['$http', 'commonData', 'Tool', function ($http, commonData, Tool) {
    var dataIF = {
      pageConf: function (year) {
        return $http.get('/road_data/pageinfo/getIndicators?year=' + year);
      },
      list: function (params) {
        return $http.get('/road_data/api/getlist', {
          params: params
        });
      },
      download: function (params) {
        return $http.get('/road_data/api/download', {
          params: params
        });
      }
    };
    return angular.extend({}, commonData, dataIF);
  }])
  ;





