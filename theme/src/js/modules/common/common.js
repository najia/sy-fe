
var templateUrl = require('./Notify.alert.html');
var echarts = require('echarts/lib/echarts');
require('../../util/pms-theme');
// 引入柱状图
require('echarts/lib/chart/bar');
// 引入提示框和标题组件
require('echarts/lib/component/tooltip');
require('echarts/lib/component/title');

angular.module("app.common", [])
    .service('Notify', ['$uibModal', function ($uibModal) {
        return {
            alert: function (alertConfig) {
                return $uibModal.open({
                    animation: true,
                    templateUrl: templateUrl,
                    backdrop: 'static',
                    windowClass: 'site-tip-dialog',
                    resolve: {
                        alertConfig: function () {
                            if (angular.isString(alertConfig)) {
                                alertConfig = {
                                    message: alertConfig
                                };
                            }
                            return alertConfig;
                        }
                    },
                    controller: ['$scope', '$uibModalInstance', 'alertConfig', function ($scope, $uibModalInstance, alertConfig) {
                        $scope.alertConfig = alertConfig;
                        $scope.ok = function () {
                            $uibModalInstance.close();
                        };
                        $scope.cancel = function () {
                            $uibModalInstance.dismiss('cancel');
                        };
                    }]
                })
            }
        };
    }])
    .service('Alert', [function () {
        return {
            show: false,
            type: '',
            msg: '',
            alert: function (msg, type) {
                this.show = !this.show;
                this.msg = msg;
                this.type = type || 'success';
            },
            close: function () {
                this.show = !this.show;
            }
        };
    }])
    .service('commonData', ['$http', function ($http) {
        var commonData = {

        };
        return commonData;
    }])
    .service('Tool', [function () {
        var tool = {
            arrayToString: function (array) {
                return array.map(function (v, k) {
                    return v.id;
                });
            },
            objIsEmpty: function (obj) {
                // null and undefined are "empty"
                if (obj == null) return true;

                // Assume if it has a length property with a non-zero value
                // that that property is correct.
                if (obj.length > 0) return false;
                if (obj.length === 0) return true;

                // Otherwise, does it have any properties of its own?
                // Note that this doesn't handle
                // toString and valueOf enumeration bugs in IE < 9
                for (var key in obj) {
                    if (hasOwnProperty.call(obj, key)) return false;
                }

                return true;
            },
            getQueryString: function (url, name) {
                var reg = new RegExp("(^|&|\\?)" + name + "=([^&]*)(&|$)", "i");
                var result = reg.exec(url);
                if (result) {
                    return decodeURIComponent(result[2]);
                } else {
                    return null;
                }
            }
        };
        return tool;
    }])
    .directive("formatDate", ['$filter', function ($filter) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                function formatDate(value, format, type) {
                    var date = new Date(value);
                    if (!value || date == 'Invalid Date') {
                        return value;
                    } else {
                        return type == true ? date : $filter('date')(date, format || 'yyyy-MM-dd', '+0800')

                    }
                }
                ngModel.$formatters.push(function (value) {
                    return formatDate(value, null, true);
                });
                ngModel.$parsers.push(function (value) {
                    return formatDate(value, null, false);
                });
            }
        }
    }])
    .service('HttpHelper', [function () {
        var HttpHelper = {
            path: function (url, obj) {
                for (var key in obj) {
                    url = url.replace('{' + key + '}', obj[key]);
                }
                return url;
            }
        };
        return HttpHelper;
    }])
    .filter('identityCode', function () { //可以注入依赖
        return function (str) {
            return str.substring(14, str.length) || '';
        }
    })
    .filter('formatePercent', function () { //可以注入依赖
        return function (str) {
            var newStr = Number(str);
            newStr = (newStr * 100).toFixed(2);
            return newStr;
        }
    })
    .filter('formateNum', function () { //可以注入依赖
        return function (str) {
            var newStr = Number(str);
            newStr = newStr.toFixed(2);
            return newStr || 0;
        }
    })
    .filter('getSignature', function () { //可以注入依赖
        return function (str, params) {
            var strs = [];
            for (var i in params) {
                strs.push(i + '=' + params[i]);
            }
            strs = strs.join('&') + '&signature=' + window.getSignature(params);
            str += '?' + strs;
            return str;
        }
    })
    .directive('lines', ['Tool', function (Tool) {
        return {
            scope: {
                id: "@",
                data: "=data",
                sTitle: "=sTitle",
                remark: "=remark"
            },
            restrict: 'E',
            template: '<div style="width:100%; margin:0 auto; height:400px;"></div>',
            replace: true,
            link: function ($scope, element, attrs, controller) {
                var option = {
                    // title : {
                    //     text: $scope.title,
                    //     subtext: $scope.remark
                    // },
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data: []
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            saveAsImage: { show: true, name: $scope.sTitle }
                        }
                    },
                    calculable: true,
                    xAxis: [],
                    yAxis: [
                        {
                            type: 'value'
                        }
                    ],
                    series: []
                };
                var myChart = null;
                // 检测数据
                $scope.$watch(function () {
                    return $scope.data;
                }, function () {
                    if (!Tool.objIsEmpty($scope.data)) {
                        option.series = $scope.data.series;
                        option.xAxis = $scope.data.xAxis;
                        option.grid = $scope.data.grid;
                        if ($scope.data.legend && $scope.data.legend.data) {
                            option.legend.data = $scope.data.legend.data;
                        }
                        if ($scope.data.yAxis) {
                            option.yAxis = $scope.data.yAxis;
                        }
                        if (!myChart) {
                            myChart = echarts.init(document.getElementById($scope.id), 'pms-theme');
                        } else {
                            myChart.clear();
                        }
                        myChart.setOption(option);
                    }
                });

            }
        };
    }])
    .directive('pie', ['Tool', function (Tool) {
        return {
            scope: {
                id: "@",
                data: "=data",
                sTitle: "=sTitle",
                remark: "=remark"
            },
            restrict: 'E',
            template: '<div style="width:100%; margin:0 auto; height:400px;"></div>',
            replace: true,
            link: function ($scope, element, attrs, controller) {
                var option = {
                    title: {
                        // text: $scope.sTitle,
                        subtext: $scope.remark,
                        x: 'right'
                    },
                    tooltip: {
                        trigger: 'item',
                        formatter: "{a} <br/>{b} : {c} ({d}%)"
                    },
                    legend: {
                        orient: 'vertical',
                        x: 'left',
                        data: []
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            saveAsImage: { show: true, name: $scope.sTitle }
                        }
                    },
                    calculable: true,
                    series: []
                };
                var myChart = null;
                // 检测数据
                $scope.$watch(function () {
                    return $scope.data;
                }, function () {
                    if (!Tool.objIsEmpty($scope.data)) {
                        option.series = $scope.data.series;
                        if ($scope.data.legend && $scope.data.legend.data) {
                            option.legend.data = $scope.data.legend.data;
                        }
                        if (!myChart) {
                            myChart = echarts.init(document.getElementById($scope.id), 'pms-theme');
                        } else {
                            myChart.clear();
                        }
                        myChart.setOption(option);
                    }
                });
            }
        };
    }])
    .directive('radar', ['Tool', function (Tool) {
        return {
            scope: {
                id: "@",
                data: "=data",
                title: "=title"
            },
            restrict: 'E',
            template: '<div style="width:100%; margin:0 auto; height:400px;"></div>',
            replace: true,
            link: function ($scope, element, attrs, controller) {
                var option = {
                    title: {
                        text: $scope.title || ''
                    },
                    tooltip: {},
                    legend: {
                        data: []
                    },
                    radar: {
                        // shape: 'circle',
                        indicator: []
                    },
                    series: []
                };
                // 检测数据
                $scope.$watch(function () {
                    return $scope.data;
                }, function () {
                    if (!Tool.objIsEmpty($scope.data)) {

                        option.series = $scope.data.series;
                        option.legend.data = $scope.data.legend;
                        option.radar.indicator = $scope.data.indicator;

                        var myChart = echarts.init(document.getElementById($scope.id), 'pms-theme');
                        myChart.setOption(option);
                    }
                });

            }
        };
    }])
    ;