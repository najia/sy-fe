/**
 * Created by Jessie on 16/11/16.
 */

'use strict';

var pageUrl = require('./log.html');
angular
    .module('app.log', [])
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/log', {
                templateUrl: pageUrl,
                controller: 'logController',
                state: 'log'
            })
        ;
    }])
    // 主活动详情
    .controller('logController', ['$scope', '$routeParams', 'logIF', 'Notify', 'Alert', '$rootScope', function($scope, $routeParams, logIF, Notify, Alert ,$rootScope) {
        var form = $scope.form = {
            offset: 1,
            count: 10
        }
        function list() {
            logIF.list(form).success(function(data) {
                if (data.status == 0 && data.data && data.data.list) {
                    $scope.list = data.data.list;
                    form.total = data.data.total;
                }
            });
        }

        $scope.pageChanged = function() {
            list();
        };
        
        list();
    }])
    .service('logIF', ['$http', 'commonData', 'Tool', function($http, commonData, Tool) {
        var logIF = {
            list: function (params) {
                return $http.get('/user/api/getOpLogList', {
                    params: params
                });
            }
        };
        return angular.extend({}, commonData, logIF);
    }])
;





