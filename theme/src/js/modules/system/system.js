/**
 * Created by Jessie on 16/11/16.
 */

'use strict';

var pciEval = require('./pciEval.html');
var iriEval = require('./iriEval.html');
var thicknessEval = require('./thicknessEval.html');
var defEval = require('./defEval.html');
var iri2Eval = require('./iri2Eval.html');
var fwdEval = require('./fwdEval.html');
var rqiEval = require('./rqiEval.html');
var pqiEval = require('./pqiEval.html');
var aadt2Eval = require('./aadt2Eval.html');
var pci2Eval = require('./pci2Eval.html');
var rqi2Eval = require('./rqi2Eval.html');
var asphaltEval = require('./asphaltEval.html');
var cementEval = require('./cementEval.html');
var infoEval = require('./infoEval.html');
var fixEval = require('./fixEval.html');
var sidebar = require('./sidebar.html')
var addModal = require('./addModal.html');
var addAadtModal = require('./addAadtModal.html');
var asphaltModal = require('./asphaltModal.html');
var cementModal = require('./cementModal.html');
var infoModal = require('./infoModal.html');
var fixModal = require('./fixModal.html');
var rqi2Modal = require('./rqi2Modal.html');
var pci2Modal = require('./pci2Modal.html');
var road = require('./road.html');
var esalEval = require('./esalEval.html');
angular
    .module('app.system', [])
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/std', {
                templateUrl: iri2Eval,
                controller: 'iri2EvalController',
                state: 'std',
                moduleName: 'remark',
                tableName: 'iri'
            })
            .when('/remark/iri', {
                templateUrl: iri2Eval,
                controller: 'iri2EvalController',
                state: 'std',
                moduleName: 'remark',
                tableName: 'iri'
            })
            .when('/remark/fwd', {
                templateUrl: fwdEval,
                controller: 'fwdEvalController',
                state: 'std',
                moduleName: 'remark',
                tableName: 'fwd'
            })
            .when('/conv/rqi', {
                templateUrl: rqiEval,
                controller: 'rqiEvalController',
                state: 'std',
                moduleName: 'conv',
                tableName: 'rqi'
            })
            .when('/conv/pqi', {
                templateUrl: pqiEval,
                controller: 'pqiEvalController',
                state: 'std',
                moduleName: 'conv',
                tableName: 'pqi'
            })
            .when('/conv/aadt', {
                templateUrl: aadt2Eval,
                controller: 'aadt2EvalController',
                state: 'std',
                moduleName: 'conv',
                tableName: 'aadt'
            })
            .when('/model/pci', {
                templateUrl: pci2Eval,
                controller: 'pci2EvalController',
                state: 'std',
                moduleName: 'model',
                tableName: 'pci'
            })
            .when('/model/rqi', {
                templateUrl: rqi2Eval,
                controller: 'rqi2EvalController',
                state: 'std',
                moduleName: 'model',
                tableName: 'rqi'
            })
            .when('/decision/asphalt', {
                templateUrl: asphaltEval,
                controller: 'asphaltEvalController',
                state: 'std',
                moduleName: 'decision',
                tableName: 'asphalt'
            })
            .when('/decision/cement', {
                templateUrl: cementEval,
                controller: 'cementEvalController',
                state: 'std',
                moduleName: 'decision',
                tableName: 'cement'
            })
            .when('/decision/info', {
                templateUrl: infoEval,
                controller: 'infoEvalController',
                state: 'std',
                moduleName: 'decision',
                tableName: 'info'
            })
            .when('/decision/fix', {
                templateUrl: fixEval,
                controller: 'fixEvalController',
                state: 'std',
                moduleName: 'decision',
                tableName: 'fix'
            })
            .when('/road', {
                templateUrl: road,
                controller: 'roadController',
                state: 'std',
                moduleName: 'road',
                tableName: 'road'
            })
            .when('/std/thickness', {
                templateUrl: thicknessEval,
                controller: 'thicknessEvalController',
                state: 'std',
                moduleName: 'std',
                tableName: 'thickness'
            })
            .when('/std/esal', {
                templateUrl: esalEval,
                state: 'std',
                moduleName: 'std',
                tableName: 'esal'
            })
        ;
    }])
    // PCI评价标准表 + RQI评价标准表
    .controller('pciEvalController', ['$scope', '$route', 'systemIF', 'Notify', 'Alert', function($scope, $route, systemIF, Notify, Alert) {
        var hdData = $scope.data = [];
        var moduleName = $route.current.moduleName;
        var tableName = $scope.tableName = $route.current.tableName;
        var tableData = $scope.tableData = [];
        function list() {
            systemIF.listHdData(moduleName, tableName).success(function(data) {
                if (data.data) {
                    hdData = $scope.hdData = data.data;
                }
            });
        }

        function listData() {
            var params = {
                tableName: tableName
            }
            systemIF.listData(moduleName, params).success(function(data) {
                if (data.data) {
                    tableData = $scope.tableData = data.data;
                }
            });
        }      
        
        $scope.saveItem = function (item) {
            if (Number(item.lev1) <= Number(item.lev2) || Number(item.lev2) <= Number(item.lev3)) {
                Alert.alert('填写的数值必须满足“优秀>良好>及格”，拒绝修改！', 'danger');
                return;
            }
            Notify.alert('是否保存当前修改？').result.then(function () {
                var dataJson = {
                    lev1: item.lev1,
                    lev2: item.lev2,
                    lev3: item.lev3
                }
                var params = {
                    tableName: tableName,
                    primary: item.sGrade,
                    dataJson: JSON.stringify(dataJson)
                }  
                systemIF.update(moduleName, params).success(function(data) {
                    item.update = false;
                    Alert.alert('操作成功');
                });
            });           
        }
        
        $scope.cancel = function (item) {
            item.update = false;
            listData();
        }

        listData();
        list();
    }])
    // IRI评价标准表
    .controller('iriEvalController', ['$scope', '$route', 'systemIF', 'Notify', 'Alert', function($scope, $route, systemIF, Notify, Alert) {
        var hdData = $scope.data = [];
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        var tableData = $scope.tableData = [];
        function list() {
            systemIF.listHdData(moduleName, tableName).success(function(data) {
                if (data.data) {
                    hdData = $scope.hdData = data.data;
                }
            });
        }

        function listData() {
            var params = {
                tableName: tableName
            }
            systemIF.listData(moduleName, params).success(function(data) {
                if (data.data) {
                    tableData = $scope.tableData = data.data;
                }
            });
        }      
        
        $scope.saveItem = function (item) {
            if (Number(item.lev1) <= Number(item.lev2) || Number(item.lev2) <= Number(item.lev3)) {
                Alert.alert('填写的数值必须满足“优秀>良好>及格”，拒绝修改！', 'danger');
                return;
            }

            Notify.alert('是否保存当前修改？').result.then(function () {
                var dataJson = {
                    lev1: item.lev1,
                    lev2: item.lev2,
                    lev3: item.lev3
                }
                var params = {
                    tableName: tableName,
                    primary: item.sGrade,
                    dataJson: JSON.stringify(dataJson)
                }  
                systemIF.update(moduleName, params).success(function(data) {
                    item.update = false;
                    Alert.alert('操作成功');
                });
            });            
        }
        $scope.cancel = function (item) {
            item.update = false;
            listData();
        }
        
        listData();
        list();
    }])
    // AADT评价标准表
    .controller('thicknessEvalController', ['$scope', '$route', 'systemIF', 'Notify', 'Alert', function($scope, $route, systemIF, Notify, Alert) {
        var hdData = $scope.data = [];
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        var tableData = $scope.tableData = [];
        function list() {
            systemIF.listHdData(moduleName, tableName).success(function(data) {
                if (data.data) {
                    hdData = $scope.hdData = data.data;
                }
            });
        }

        function listData() {
            var params = {
                tableName: tableName
            }
            systemIF.listData(moduleName, params).success(function(data) {
                if (data.data) {
                    tableData = $scope.tableData = data.data;
                }
            });
        }      
        
        $scope.saveItem = function (item) {
            if (Number(item.medium) <= Number(item.thin) || Number(item.thick) <= Number(item.medium)) {
                Alert.alert('填写的数值必须满足“较厚>中>薄”，拒绝修改！', 'danger');
                return;
            }
            Notify.alert('是否保存当前修改？').result.then(function () {
                var dataJson = {
                    thin: item.thin,
                    thick: item.thick,
                    medium: item.medium
                }
                var params = {
                    tableName: tableName,
                    primary: '',
                    dataJson: JSON.stringify(dataJson)
                }  
                systemIF.update(moduleName, params).success(function(data) {
                    item.update = false;
                    Alert.alert('操作成功');
                });
            });            
        }
        $scope.cancel = function (item) {
            item.update = false;
            listData();
        }
        
        listData();
        list();
    }])
    // DEF评价标准表
    .controller('defEvalController', ['$scope', '$route', 'systemIF', 'Notify', 'Alert', function($scope, $route, systemIF, Notify, Alert) {
        var hdData = $scope.data = [];
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        var tableData = $scope.tableData = [];
        function list() {
            systemIF.listHdData(moduleName, tableName).success(function(data) {
                if (data.data) {
                    hdData = $scope.hdData = data.data;
                }
            });
        }

        function listData() {
            var params = {
                tableName: tableName
            }
            systemIF.listData(moduleName, params).success(function(data) {
                if (data.data) {
                    tableData = $scope.tableData = data.data;
                }
            });
        }      
        
        $scope.save = function (item) {
            var lev1 = Number(tableData[0].lev1);
            var lev2 = Number(tableData[0].lev2);
            var lev3 = Number(tableData[0].lev3);
            var lev4 = Number(tableData[0].lev4);

            for(var i = 0; i< tableData.length; i++) {
                if (i > 0 && (Number(tableData[i].lev1) >= lev1 || Number(tableData[i].lev2) >= lev2 || Number(tableData[i].lev3) >= lev3 || Number(tableData[i].lev4) >= lev4)) {
                    Alert.alert('填写的数值必须满足“特重<重<中<轻<很轻”，拒绝修改！', 'danger');
                    return false;
                }
                if (Number(tableData[i].lev1) >= Number(tableData[i].lev2) || Number(tableData[i].lev3) >= Number(tableData[i].lev4)) {
                    Alert.alert('临界上值必须低于临界下值！', 'danger');
                    return false;
                }
                lev1 = tableData[i].lev1;
                lev2 = tableData[i].lev2;
                lev3 = tableData[i].lev3;
                lev4 = tableData[i].lev4;
            }    
            Notify.alert('是否保存当前修改？').result.then(function () {        
                tableData.map(function (v, k) {
                    var dataJson = {
                        lev1: v.lev1,
                        lev2: v.lev2,
                        lev3: v.lev3,
                        lev4: v.lev4
                    }
                    var params = {
                        tableName: tableName,
                        primary: v.sTrafficLevel,
                        dataJson: JSON.stringify(dataJson)
                    }
                    systemIF.update(moduleName, params).success(function(data) {
                        $scope.isSave = false;
                        if (k == tableData.length - 1) {
                            Alert.alert('操作成功');
                        }
                    });
                });
            });            
        }
        $scope.cancel = function () {
            $scope.isSave = false;
            listData();
        }
        
        listData();
        list();
    }])
    .controller('iri2EvalController', ['$scope', '$route', 'systemIF', 'Notify', 'Alert', 'addModal', function($scope, $route, systemIF, Notify, Alert, addModal) {
        var hdData = $scope.data = [];
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        var tableData = $scope.tableData = [];
        function list() {
            systemIF.listHdData(moduleName, tableName).success(function(data) {
                if (data.data) {
                    hdData = $scope.hdData = data.data;
                }
            });
        }

        function listData() {
            var params = {
                tableName: tableName
            }
            systemIF.listData(moduleName, params).success(function(data) {
                if (data.data) {
                    tableData = $scope.tableData = data.data;
                }
            });
        }      
        
        $scope.saveItem = function (item) {
            if (!item.sId.length || !item.fParamA.length || !item.fParamB.length) {
                Alert.alert('ID，参数A，参数B，不能为空！', 'danger');
                return;
            }

            Notify.alert('是否保存当前修改？').result.then(function () {
                var dataJson = {
                    sId: item.sId,
                    fParamA: item.fParamA,
                    fParamB: item.fParamB,
                    sComment: item.sComment,
                    isCurrent: item.isCurrent
                }
                var params = {
                    tableName: tableName,
                    primary: item.sId,
                    dataJson: JSON.stringify(dataJson)
                }
                systemIF.update(moduleName, params).success(function(data) {
                    item.update = false;
                    Alert.alert('操作成功');
                });
            });            
        }

        $scope.enable = function (item) {
            if (item.isCurrent == 0) {
                Notify.alert('是否采用该套标定公式？').result.then(function () {
                    var params = {
                        tableName: tableName,
                        primary: item.sId
                    }
                    systemIF.enable(moduleName, params).success(function(data) {
                        if (data.status == 0) {
                            listData();
                        }
                    });
                });
            }           
        }

        $scope.delete = function (id) {
            Notify.alert('是否删除该标定公式？').result.then(function () {
                var params = {
                    tableName: tableName,
                    primary: id
                }
                systemIF.delete(moduleName, params).success(function(data) {
                    if (data.status == 0) {
                        Alert.alert('删除成功');
                        listData();
                    }
                });
            });
        }
        $scope.add = function () {
            addModal.exec().result.then(function(msg) {
                Alert.alert(msg);
                listData();
            });
        }
        
        $scope.cancel = function (item) {
            item.update = false;
            listData();
        }

        listData();
        list();
    }])
    .controller('fwdEvalController', ['$scope', '$route', 'systemIF', 'Notify', 'Alert', 'addModal', function($scope, $route, systemIF, Notify, Alert, addModal) {
        var hdData = $scope.data = [];
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        var tableData = $scope.tableData = [];
        function list() {
            systemIF.listHdData(moduleName, tableName).success(function(data) {
                if (data.data) {
                    hdData = $scope.hdData = data.data;
                }
            });
        }

        function listData() {
            var params = {
                tableName: tableName
            }
            systemIF.listData(moduleName, params).success(function(data) {
                if (data.data) {
                    tableData = $scope.tableData = data.data;
                }
            });
        }      
        
        $scope.saveItem = function (item) {
            if (!item.sId.length || !item.fParamA.length || !item.fParamB.length) {
                Alert.alert('ID，参数A，参数B，不能为空！', 'danger');
                return;
            }

            Notify.alert('是否保存当前修改？').result.then(function () {
                var dataJson = {
                    sId: item.sId,
                    fParamA: item.fParamA,
                    fParamB: item.fParamB,
                    sComment: item.sComment,
                    isCurrent: item.isCurrent
                }
                var params = {
                    tableName: tableName,
                    primary: item.sId,
                    dataJson: JSON.stringify(dataJson)
                }
                systemIF.update(moduleName, params).success(function(data) {
                    item.update = false;
                    Alert.alert('操作成功');
                });
            });            
        }

        $scope.enable = function (item) {
            if (item.isCurrent == 0) {
                Notify.alert('是否采用该套标定公式？').result.then(function () {
                    var params = {
                        tableName: tableName,
                        primary: item.sId
                    }
                    systemIF.enable(moduleName, params).success(function(data) {
                        if (data.status == 0) {
                            listData();
                        }
                    });
                });
            }           
        }

        $scope.delete = function (id) {
            Notify.alert('是否删除该标定公式？').result.then(function () {
                var params = {
                    tableName: tableName,
                    primary: id
                }
                systemIF.delete(moduleName, params).success(function(data) {
                    if (data.status == 0) {
                        Alert.alert('删除成功');
                        listData();
                    }
                });
            });
        }
        $scope.add = function () {
            addModal.exec().result.then(function(msg) {
                Alert.alert(msg);
                listData();
            });
        }
        
        $scope.cancel = function (item) {
            item.update = false;
            listData();
        }

        listData();
        list();
    }])
    .controller('rqiEvalController', ['$scope', '$route', 'systemIF', 'Notify', 'Alert', function($scope, $route, systemIF, Notify, Alert) {
        var hdData = $scope.data = [];
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        var tableData = $scope.tableData = [];
        function list() {
            systemIF.listHdData(moduleName, tableName).success(function(data) {
                if (data.data) {
                    hdData = $scope.hdData = data.data;
                }
            });
        }

        function listData() {
            var params = {
                tableName: tableName
            }
            systemIF.listData(moduleName, params).success(function(data) {
                if (data.data) {
                    tableData = $scope.tableData = data.data;
                }
            });
        }      
        
        $scope.saveItem = function (item) {
            if (!item.A.length || !item.B.length) {
                Alert.alert('内容不能为空', 'danger');
                return;
            }
            Notify.alert('是否保存当前修改？').result.then(function () {
                var dataJson = {
                    A: item.A,
                    B: item.B
                }
                var params = {
                    tableName: tableName,
                    primary: '',
                    dataJson: JSON.stringify(dataJson)
                }
                systemIF.update(moduleName, params).success(function(data) {
                    item.update = false;
                    Alert.alert('操作成功');
                });
            });            
        }
        $scope.cancel = function (item) {
            item.update = false;
            listData();
        }
        
        listData();
        list();
    }])
    .controller('pqiEvalController', ['$scope', '$route', 'systemIF', 'Notify', 'Alert', function($scope, $route, systemIF, Notify, Alert) {
        var hdData = $scope.data = [];
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        var tableData = $scope.tableData = [];
        function list() {
            systemIF.listHdData(moduleName, tableName).success(function(data) {
                if (data.data) {
                    hdData = $scope.hdData = data.data;
                }
            });
        }

        function listData() {
            var params = {
                tableName: tableName
            }
            systemIF.listData(moduleName, params).success(function(data) {
                if (data.data) {
                    tableData = $scope.tableData = data.data;
                }
            });
        }      
        
        $scope.saveItem = function (item) {
            if (!item.PCI.length || !item.RQI.length) {
                Alert.alert('内容不能为空', 'danger');
                return;
            }
            Notify.alert('是否保存当前修改？').result.then(function () {
                var dataJson = {
                    PCI: item.PCI,
                    RQI: item.RQI
                }
                var params = {
                    tableName: tableName,
                    primary: item.sGrade,
                    dataJson: JSON.stringify(dataJson)
                }  
                systemIF.update(moduleName, params).success(function(data) {
                    item.update = false;
                    Alert.alert('操作成功');
                });
            });            
        }
        $scope.cancel = function (item) {
            item.update = false;
            listData();
        }
        
        listData();
        list();
    }])
    .controller('aadt2EvalController', ['$scope', '$route', 'systemIF', 'Notify', 'Alert', 'addAadtModal', function($scope, $route, systemIF, Notify, Alert, addAadtModal) {
        var hdData = $scope.data = [];
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        var tableData = $scope.tableData = [];
        function list() {
            systemIF.listHdData(moduleName, tableName).success(function(data) {
                if (data.data) {
                    hdData = $scope.hdData = data.data;
                }
            });
        }

        function listData() {
            var params = {
                tableName: tableName
            }
            systemIF.listData(moduleName, params).success(function(data) {
                if (data.data) {
                    tableData = $scope.tableData = data.data;
                }
            });
        }      
        
        $scope.saveItem = function (item) {
            if (!item.AADT.length || !item.ESAL.length) {
                Alert.alert('内容不能为空', 'danger');
                return;
            }
            Notify.alert('是否保存当前修改？').result.then(function () {
                var dataJson = {
                    AADT: item.AADT,
                    ESAL: item.ESAL
                }
                var params = {
                    tableName: tableName,
                    primary: item.sGrade,
                    dataJson: JSON.stringify(dataJson)
                }  
                systemIF.update(moduleName, params).success(function(data) {
                    item.update = false;
                    Alert.alert('操作成功');
                });
            });            
        }

        $scope.delete = function (id) {
            Notify.alert('是否删除该标定公式？').result.then(function () {
                var params = {
                    tableName: tableName,
                    primary: id
                }
                systemIF.delete(moduleName, params).success(function(data) {
                    if (data.status == 0) {
                        Alert.alert('删除成功');
                        listData();
                    }
                });
            });
        }
        
        $scope.add = function () {
            addAadtModal.exec().result.then(function(msg) {
                Alert.alert(msg);
                listData();
            });
        }

        $scope.cancel = function (item) {
            item.update = false;
            listData();
        }
        
        listData();
        list();
    }])
    .controller('pci2EvalController', ['$scope', '$route', 'systemIF', 'Notify', 'Alert', 'pci2Modal', function($scope, $route, systemIF, Notify, Alert, pci2Modal) {
        var hdData = $scope.data = [];
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        var tableData = $scope.tableData = [];
        function list() {
            systemIF.listHdData(moduleName, tableName).success(function(data) {
                if (data.data) {
                    hdData = $scope.hdData = data.data;
                }
            });
        }

        function listData() {
            var params = {
                tableName: tableName
            }
            systemIF.listData(moduleName, params).success(function(data) {
                if (data.data) {
                    tableData = $scope.tableData = data.data;
                }
            });
        }      
        
        $scope.saveItem = function (item) {
            if (!item.fParamA.length || !item.fParamB.length || !item.sComment.length) {
                Alert.alert('内容不能为空', 'danger');
                return;
            }
            Notify.alert('是否保存当前修改？').result.then(function () {
                var dataJson = {
                    fParamA: item.fParamA,
                    fParamB: item.fParamB,
                    sFaceType: item.sFaceType,
                    sBaseType: item.sBaseType,
                    sGrade: item.sGrade,
                    sComment: item.sComment
                }
                var params = {
                    tableName: tableName,
                    primary: item.sId,
                    dataJson: JSON.stringify(dataJson)
                } 
                systemIF.update(moduleName, params).success(function(data) {
                    item.update = false;
                    Alert.alert('操作成功');
                });
            });            
        }

        $scope.cancel = function (item) {
            item.update = false;
            listData();
        }

        $scope.delete = function (id) {
            Notify.alert('是否删除该模型参数？').result.then(function () {
                var params = {
                    tableName: tableName,
                    primary: id
                }
                systemIF.delete(moduleName, params).success(function(data) {
                    if (data.status == 0) {
                        Alert.alert('删除成功');
                        listData();
                    }
                });
            });
        }

        $scope.add = function () {
            pci2Modal.exec(hdData).result.then(function(msg) {
                Alert.alert(msg);
                listData();
            });
        }
        
        listData();
        list();
    }])
    .controller('rqi2EvalController', ['$scope', '$route', 'systemIF', 'Notify', 'Alert', 'rqi2Modal', function($scope, $route, systemIF, Notify, Alert, rqi2Modal) {
        var hdData = $scope.data = [];
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        var tableData = $scope.tableData = [];
        function list() {
            systemIF.listHdData(moduleName, tableName).success(function(data) {
                if (data.data) {
                    hdData = $scope.hdData = data.data;
                }
            });
        }

        function listData() {
            var params = {
                tableName: tableName
            }
            systemIF.listData(moduleName, params).success(function(data) {
                if (data.data) {
                    tableData = $scope.tableData = data.data;
                }
            });
        }      
        
        $scope.saveItem = function (item) {
            if (!item.fParamA.length || !item.fParamB.length || !item.sComment.length || !item.sFaceType.length || !item.sBaseType.length || !item.sFaceThick.length || !item.ESAL.length) {
                Alert.alert('内容不能为空', 'danger');
                return;
            }
            Notify.alert('是否保存当前修改？').result.then(function () {
                var dataJson = {
                    fParamA: item.fParamA,
                    fParamB: item.fParamB,
                    sComment: item.sComment,
                    sFaceType: item.sFaceType,
                    sBaseType: item.sBaseType,
                    sFaceThick: item.sFaceThick,
                    ESAL: item.ESAL,
                    isDefault: item.isDefault
                }
                var params = {
                    tableName: tableName,
                    primary: item.sId,
                    dataJson: JSON.stringify(dataJson)
                } 
                systemIF.update(moduleName, params).success(function(data) {
                    item.update = false;
                    Alert.alert('操作成功');
                });
            });            
        }

        $scope.cancel = function (item) {
            item.update = false;
            listData();
        }

        $scope.delete = function (id) {
            Notify.alert('是否删除该模型参数？').result.then(function () {
                var params = {
                    tableName: tableName,
                    primary: id
                }
                systemIF.delete(moduleName, params).success(function(data) {
                    if (data.status == 0) {
                        Alert.alert('删除成功');
                        listData();
                    }
                });
            });
        }

        $scope.add = function () {
            rqi2Modal.exec(hdData).result.then(function(msg) {
                Alert.alert(msg);
                listData();
            });
        }
        
        listData();
        list();
    }])
    .controller('asphaltEvalController', ['$scope', '$route', 'systemIF', 'Notify', 'Alert', 'asphaltModal', 'Tool', function($scope, $route, systemIF, Notify, Alert, asphaltModal, Tool) {
        var hdData = $scope.data = [];
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        var tableData = $scope.tableData = [];

        var listParams = $scope.listParams = {
            offset: 1,
            count: 10,
            total: 0,
            tableName: tableName
        };

        function changeToArray(data, index) {
            var result = [];

            for (var i = 0; i < data.length; i ++) {
                var getResult = getObj(hdData[index].values, data[i]);
                if (getResult) {
                    result.push(getResult);
                }
            }
            console.log(result);
            return result;
        };

        function decision () {

        }
        
        function listData() {
            systemIF.listData(moduleName, listParams).success(function(data) {
                if (data.data.list) {
                    tableData = $scope.tableData = data.data.list;
                    for (var i = 0; i < tableData.length; i++) {
                        tableData[i].PSSI = changeToArray(tableData[i].PSSI, 2);
                        // tableData[i].DEF = changeToArray(tableData[i].DEF, 2);
                        tableData[i].PCI = changeToArray(tableData[i].PCI, 3);
                        tableData[i].RQI = changeToArray(tableData[i].RQI, 4);
                        tableData[i].AADT = changeToArray(tableData[i].AADT, 5);
                        tableData[i].decisionA = changeToArray(tableData[i].decisionA, 6);
                    }
                    
                    listParams.total = data.data.total;
                }
            });
        } 

        $scope.pageChanged = function() {
            listData();
        };

        function changeToObj(data) {
            const result = [];
            for (var key in data) {
                result.push({
                    id: key,
                    label: data[key],
                })
            }
            return result;
        }

        function list() {
            systemIF.listHdData(moduleName, tableName).success(function(data) {
                if (data.data) {
                    hdData = $scope.hdData = data.data;
                    angular.forEach(hdData, function (v) {
                        if (v.sName == 'decisionA') {
                            v.values = changeToObj(v.values);
                            console.log(v.values, '123');
                        }
                    });
                    listData();
                }
            });
        }

        function getObj(data, id) {
           return data.filter(function(v, k) {
                if (v.id == id) {
                    return v;
                }
            })[0];
        }
        
        $scope.saveItem = function (item) {
            if (!item.sBase.length || !item.DEF.length || !item.PCI.length || !item.RQI.length || !item.AADT.length || !item.decisionA.length || !item.decisionB.length|| !item.decisionC.length) {
                Alert.alert('内容不能为空', 'danger');
                return;
            }            
            Notify.alert('是否保存当前修改？').result.then(function () {
                var dataJson = {
                    sBase: item.sBase,
                    DEF: Tool.arrayToString(item.DEF).join(','),
                    PCI: Tool.arrayToString(item.PCI).join(','),
                    RQI: Tool.arrayToString(item.RQI).join(','),
                    AADT: Tool.arrayToString(item.AADT).join(','),
                    decisionA: item.decisionA,
                    decisionB: item.decisionB,
                    decisionC: item.decisionC
                }
                var params = {
                    tableName: tableName,
                    primary: item.sSignature,
                    dataJson: JSON.stringify(dataJson)
                } 
                systemIF.update(moduleName, params).success(function(data) {
                    item.update = false;
                    Alert.alert('操作成功');
                });
            });            
        }

        $scope.cancel = function (item) {
            item.update = false;
            listData();
        }

        $scope.delete = function (id) {
            Notify.alert('是否删除该分支？').result.then(function () {
                var params = {
                    tableName: tableName,
                    primary: id
                }
                systemIF.delete(moduleName, params).success(function(data) {
                    if (data.status == 0) {
                        Alert.alert('删除成功');
                        listData();
                    }
                });
            });
        }

        $scope.add = function () {
            asphaltModal.exec(hdData).result.then(function(msg) {
                Alert.alert(msg);
                listData();
            });
        }
        list();
    }])
    .controller('cementEvalController', ['$scope', '$route', 'systemIF', 'Notify', 'Alert', 'cementModal', 'Tool', function($scope, $route, systemIF, Notify, Alert, cementModal, Tool) {
        var hdData = $scope.data = [];
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        var tableData = $scope.tableData = [];


        function changeToObj(data) {
            const result = [];
            for (var key in data) {
                result.push({
                    key: key,
                    value: data[key],
                })
            }
            return result;
        }
        
        function list() {
            systemIF.listHdData(moduleName, tableName).success(function(data) {
                if (data.data) {
                    hdData = $scope.hdData = data.data;
                    angular.forEach(hdData, function (v) {
                        if (v.sName == 'decisionA') {
                            v.values = changeToObj(v.values);
                        }
                    });
                    listData();
                }
            });
        }

        var listParams = $scope.listParams = {
            offset: 1,
            count: 10,
            total: 0,
            tableName: tableName
        };

        function changeToArray(data, index) {
            var result = [];

            for (var i = 0; i < data.length; i ++) {
                var getResult = getObj(hdData[index].values, data[i]);
                if (getResult) {
                    result.push(getResult);
                }
            }
            return result;
        };
        
        function getObj(data, id) {
            return data.filter(function(v, k) {
                if (v.id == id) {
                    return v;
                }
            })[0];
        }

        function listData() {
            systemIF.listData(moduleName, listParams).success(function(data) {
                if (data.data) {
                    tableData = $scope.tableData = data.data.list;
                    for (var i = 0; i < tableData.length; i++) {
                        tableData[i].PCI = changeToArray(tableData[i].PCI, 1);
                        tableData[i].RQI = changeToArray(tableData[i].RQI, 2);
                        tableData[i].AADT = changeToArray(tableData[i].AADT, 3);

                        listParams.total = data.data.total;
                    }
                }
            });
        }

        $scope.pageChanged = function() {
            listData();
        };    
        
        $scope.saveItem = function (item) {
            if (!item.PCI.length || !item.RQI.length || !item.AADT.length || !item.decisionA.length) {
                Alert.alert('内容不能为空', 'danger');
                return;
            }
            Notify.alert('是否保存当前修改？').result.then(function () {
                var dataJson = {
                    PCI: Tool.arrayToString(item.PCI).join(','),
                    RQI: Tool.arrayToString(item.RQI).join(','),
                    AADT: Tool.arrayToString(item.AADT).join(','),
                    decisionA: item.decisionA
                }
                var params = {
                    tableName: tableName,
                    primary: item.sSignature,
                    dataJson: JSON.stringify(dataJson)
                } 
                systemIF.update(moduleName, params).success(function(data) {
                    item.update = false;
                    Alert.alert('操作成功');
                });
            });            
        }

        $scope.cancel = function (item) {
            item.update = false;
            listData();
        }

        $scope.delete = function (id) {
            Notify.alert('是否删除该分支？').result.then(function () {
                var params = {
                    tableName: tableName,
                    primary: id
                }
                systemIF.delete(moduleName, params).success(function(data) {
                    if (data.status == 0) {
                        Alert.alert('删除成功');
                        listData();
                    }
                });
            });
        }

        $scope.add = function () {
            cementModal.exec(hdData).result.then(function(msg) {
                Alert.alert(msg);
                listData();
            });
        }
        list();
    }])
    .controller('infoEvalController', ['$scope', '$route', 'systemIF', 'Notify', 'Alert', 'infoModal', function($scope, $route, systemIF, Notify, Alert, infoModal) {
        var hdData = $scope.data = [];
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        var tableData = $scope.tableData = [];

        var listParams = $scope.listParams = {
            offset: 1,
            count: 10,
            total: 0,
            tableName: tableName
        };


        function list() {
            systemIF.listHdData(moduleName, tableName).success(function(data) {
                if (data.data) {
                    hdData = $scope.hdData = data.data;
                }
            });
        }

        function listData() {
            systemIF.listData(moduleName, listParams).success(function(data) {
                if (data.data.list) {
                    tableData = $scope.tableData = data.data.list;

                    listParams.total = data.data.total;
                }
            });
        }      
        
        $scope.pageChanged = function() {
            listData();
        };
        
        $scope.saveItem = function (item) {
            if (!item.sCategory.length || !item.decisionFee.length) {
                Alert.alert('内容不能为空', 'danger');
                return;
            }
            Notify.alert('是否保存当前修改？').result.then(function () {
                var dataJson = {
                    sCategory: item.sCategory,
                    decisionFee: item.decisionFee
                }
                var params = {
                    tableName: tableName,
                    primary: item.id,
                    dataJson: JSON.stringify(dataJson)
                } 
                systemIF.update(moduleName, params).success(function(data) {
                    item.update = false;
                    Alert.alert('操作成功');
                });
            });            
        }

        $scope.cancel = function (item) {
            item.update = false;
            listData();
        }

        $scope.delete = function (id) {
            Notify.alert('是否删除该对策？').result.then(function () {
                var params = {
                    tableName: tableName,
                    primary: id
                }
                systemIF.delete(moduleName, params).success(function(data) {
                    if (data.status == 0) {
                        Alert.alert('删除成功');
                        listData();
                    }
                });
            });
        }

        $scope.add = function () {
            infoModal.exec(hdData).result.then(function(msg) {
                Alert.alert(msg);
                listData();
            });
        }
        
        listData();
        list();
    }])
    .controller('fixEvalController', ['$scope', '$route', 'systemIF', 'Notify', 'Alert', 'fixModal', function($scope, $route, systemIF, Notify, Alert, fixModal) {
        var hdData = $scope.data = [];
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        var tableData = $scope.tableData = [];

        var listParams = $scope.listParams = {
            offset: 1,
            count: 10,
            total: 0
        };

        function list() {
            systemIF.listHdData(moduleName, tableName, listParams).success(function(data) {
                if (data.data) {
                    hdData = $scope.hdData = data.data;
                }
            });
        }

        function listData() {
            var params = {
                tableName: tableName
            }
            systemIF.listData(moduleName, params).success(function(data) {
                if (data.data) {
                    tableData = $scope.tableData = data.data.list;
                    listParams.total = data.data.total;
                }
            });
        }      

        $scope.pageChanged = function() {
            listData();
        };
        
        $scope.saveItem = function (item) {
            if (!item.minFee.length) {
                Alert.alert('内容不能为空', 'danger');
                return;
            }
            Notify.alert('是否保存当前修改？').result.then(function () {
                var dataJson = {
                    minFee: item.minFee
                }
                var params = {
                    tableName: tableName,
                    primary: item.sGrade,
                    dataJson: JSON.stringify(dataJson)
                } 
                systemIF.update(moduleName, params).success(function(data) {
                    item.update = false;
                    Alert.alert('操作成功');
                });
            });            
        }

        $scope.cancel = function (item) {
            item.update = false;
            listData();
        }

        $scope.delete = function (id) {
            Notify.alert('是否删除该对策？').result.then(function () {
                var params = {
                    tableName: tableName,
                    primary: id
                }
                systemIF.delete(moduleName, params).success(function(data) {
                    if (data.status == 0) {
                        Alert.alert('删除成功');
                        listData();
                    }
                });
            });
        }

        $scope.add = function () {
            fixModal.exec(hdData).result.then(function(msg) {
                Alert.alert(msg);
                listData();
            });
        }
        
        listData();
        list();
    }])
    .controller('addModalController', ['$scope', '$route', '$uibModalInstance', 'systemIF', 'item', function ($scope, $route, $uibModalInstance, systemIF, item) {
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        $scope.form = {
            isCurrent: '0'
        }
        $scope.save = function () {
            if ((moduleName == 'model' && tableName == 'pci') || (moduleName == 'model' && tableName == 'rqi')) {
                delete $scope.form.isCurrent
            }
            var params = {
                tableName: tableName,
                dataJson: JSON.stringify($scope.form)
            }
            systemIF.add(moduleName, params).success(function(data) {
                if (data.status == 0) {
                    $uibModalInstance.close('添加成功！');
                }
            });
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('rqi2ModalController', ['$scope', '$route', '$uibModalInstance', 'systemIF', 'item', function ($scope, $route, $uibModalInstance, systemIF, item) {
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        $scope.hdData = item;
        $scope.form = {}
        $scope.save = function () {
            var params = {
                tableName: tableName,
                dataJson: JSON.stringify($scope.form)
            }
            systemIF.add(moduleName, params).success(function(data) {
                if (data.status == 0) {
                    $uibModalInstance.close('添加成功！');
                }
            });
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('pci2ModalController', ['$scope', '$route', '$uibModalInstance', 'systemIF', 'item', function ($scope, $route, $uibModalInstance, systemIF, item) {
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        $scope.hdData = item;
        $scope.form = {}
        $scope.save = function () {
            var params = {
                tableName: tableName,
                dataJson: JSON.stringify($scope.form)
            }
            systemIF.add(moduleName, params).success(function(data) {
                if (data.status == 0) {
                    $uibModalInstance.close('添加成功！');
                }
            });
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('addAadtModalController', ['$scope', '$route', '$uibModalInstance', 'systemIF', 'item', function ($scope, $route, $uibModalInstance, systemIF, item) {
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        $scope.form = {};
        $scope.save = function () {
            var params = {
                tableName: tableName,
                dataJson: JSON.stringify($scope.form)
            }
            systemIF.add(moduleName, params).success(function(data) {
                if (data.status == 0) {
                    $uibModalInstance.close('添加成功！');
                }
            });
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('addThicknessModalController', ['$scope', '$route', '$uibModalInstance', 'systemIF', 'item', function ($scope, $route, $uibModalInstance, systemIF, item) {
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        $scope.form = {};
        $scope.save = function () {
            var params = {
                tableName: tableName,
                dataJson: JSON.stringify($scope.form)
            }
            systemIF.add(moduleName, params).success(function(data) {
                if (data.status == 0) {
                    $uibModalInstance.close('添加成功！');
                }
            });
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('infoModalController', ['$scope', '$route', '$uibModalInstance', 'systemIF', 'item', function ($scope, $route, $uibModalInstance, systemIF, item) {
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        $scope.form = {};
        $scope.save = function () {
            var params = {
                tableName: tableName,
                dataJson: JSON.stringify($scope.form)
            }
            systemIF.add(moduleName, params).success(function(data) {
                if (data.status == 0) {
                    $uibModalInstance.close('添加成功！');
                }
            });
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('fixModalController', ['$scope', '$route', '$uibModalInstance', 'systemIF', 'item', function ($scope, $route, $uibModalInstance, systemIF, item) {
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        $scope.form = {};
        $scope.save = function () {
            var params = {
                tableName: tableName,
                dataJson: JSON.stringify($scope.form)
            }
            systemIF.add(moduleName, params).success(function(data) {
                if (data.status == 0) {
                    $uibModalInstance.close('添加成功！');
                }
            });
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('asphaltModalController', ['$scope', '$route', '$uibModalInstance', 'systemIF', 'item', 'Tool', function ($scope, $route, $uibModalInstance, systemIF, item, Tool) {
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        $scope.hdData = item;
        $scope.form = {};

        $scope.save = function () {
            $scope.form.DEF = Tool.arrayToString($scope.form.DEF).join(',');
            $scope.form.PCI = Tool.arrayToString($scope.form.PCI).join(',');
            $scope.form.RQI = Tool.arrayToString($scope.form.RQI).join(',');
            $scope.form.AADT = Tool.arrayToString($scope.form.AADT).join(',');
            var params = {
                tableName: tableName,
                dataJson: JSON.stringify($scope.form)
            }
            systemIF.add(moduleName, params).success(function(data) {
                if (data.status == 0) {
                    $uibModalInstance.close('添加成功！');
                }
            });
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('cementModalController', ['$scope', '$route', '$uibModalInstance', 'systemIF', 'item', 'Tool', function ($scope, $route, $uibModalInstance, systemIF, item, Tool) {
        var moduleName = $route.current.moduleName;
        var tableName = $route.current.tableName;
        $scope.hdData = item;
        $scope.form = {};
        $scope.save = function () {
            $scope.form.PCI = Tool.arrayToString($scope.form.PCI).join(',');
            $scope.form.RQI = Tool.arrayToString($scope.form.RQI).join(',');
            $scope.form.AADT = Tool.arrayToString($scope.form.AADT).join(',');

            var params = {
                tableName: tableName,
                dataJson: JSON.stringify($scope.form)
            }
            systemIF.add(moduleName, params).success(function(data) {
                if (data.status == 0) {
                    $uibModalInstance.close('添加成功！');
                }
            });
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('roadController', ['$scope', '$route', 'systemIF', function($scope, $route, systemIF) {
        $scope.hdData = [{
            sLable: '评定指标',
            sName: "evaluate",
        }, {
            sLable: '优',
            sName: "perfect",
        }, {
            sLable: '良',
            sName: "great",
        }, {
            sLable: '中',
            sName: "medium",
        }, {
            sLable: '次',
            sName: "second",
        }, {
            sLable: '差',
            sName: "bad",
        }];
        $scope.tableData = [{
            evaluate: 'MQI',
            perfect: '≥90',
            great: '≥80, <90',
            medium: '≥70, <80',
            second: '≥60, <70',
            bad: '<60',
        }];
        $scope.tableData2 = [{
            evaluate: 'SCI、PQI、BCI、TCI',
            perfect: '≥90',
            great: '≥80, <90',
            medium: '≥70, <80',
            second: '≥60, <70',
            bad: '<60',
        }, {
            evaluate: 'PCI、RQI、RDI、PBI、PWI、SRI、PSSI',
            perfect: '≥90',
            great: '≥80, <90',
            medium: '≥70, <80',
            second: '≥60, <70',
            bad: '<60',
        }];
    }])
    .service('addModal', ['$uibModal', function ($uibModal) {
        return {
            exec: function (item) {
                return $uibModal.open({
                    backdrop: 'static',
                    templateUrl: addModal,
                    controller: 'addModalController',
                    resolve: {
                        item: function () {
                            if (item) {
                                return item;
                            }
                        }
                    }
                });
            }
        }
    }])
    .service('rqi2Modal', ['$uibModal', function ($uibModal) {
        return {
            exec: function (item) {
                return $uibModal.open({
                    backdrop: 'static',
                    templateUrl: rqi2Modal,
                    controller: 'rqi2ModalController',
                    resolve: {
                        item: function () {
                            if (item) {
                                return item;
                            }
                        }
                    }
                });
            }
        }
    }])
    .service('pci2Modal', ['$uibModal', function ($uibModal) {
        return {
            exec: function (item) {
                return $uibModal.open({
                    backdrop: 'static',
                    templateUrl: pci2Modal,
                    controller: 'pci2ModalController',
                    resolve: {
                        item: function () {
                            if (item) {
                                return item;
                            }
                        }
                    }
                });
            }
        }
    }])
    .service('infoModal', ['$uibModal', function ($uibModal) {
        return {
            exec: function (item) {
                return $uibModal.open({
                    backdrop: 'static',
                    templateUrl: infoModal,
                    controller: 'infoModalController',
                    resolve: {
                        item: function () {
                            if (item) {
                                return item;
                            }
                        }
                    }
                });
            }
        }
    }])
    .service('fixModal', ['$uibModal', function ($uibModal) {
        return {
            exec: function (item) {
                return $uibModal.open({
                    backdrop: 'static',
                    templateUrl: fixModal,
                    controller: 'fixModalController',
                    resolve: {
                        item: function () {
                            if (item) {
                                return item;
                            }
                        }
                    }
                });
            }
        }
    }])
    .service('addAadtModal', ['$uibModal', function ($uibModal) {
        return {
            exec: function (item) {
                return $uibModal.open({
                    backdrop: 'static',
                    templateUrl: addAadtModal,
                    controller: 'addAadtModalController',
                    resolve: {
                        item: function () {
                            if (item) {
                                return item;
                            }
                        }
                    }
                });
            }
        }
    }])
    .service('asphaltModal', ['$uibModal', function ($uibModal) {
        return {
            exec: function (item) {
                return $uibModal.open({
                    backdrop: 'static',
                    templateUrl: asphaltModal,
                    controller: 'asphaltModalController',
                    resolve: {
                        item: function () {
                            if (item) {
                                return item;
                            }
                        }
                    }
                });
            }
        }
    }])
    .service('cementModal', ['$uibModal', function ($uibModal) {
        return {
            exec: function (item) {
                return $uibModal.open({
                    backdrop: 'static',
                    templateUrl: cementModal,
                    controller: 'cementModalController',
                    resolve: {
                        item: function () {
                            if (item) {
                                return item;
                            }
                        }
                    }
                });
            }
        }
    }])
    .value('sidebars', (function () {
        var sidebars = [
            {
                label: '标定参数',
                type: 'remark',
                sub: [
                    {
                        label: 'IRI标定表',
                        type: 'iri'
                    },
                    {
                        label: 'FWD标定表',
                        type: 'fwd'
                    },
                ]
            },
            {
                label: '评价标准表',
                type: 'std',
                sub: [
                    {
                        label: '面层厚度评价标准表',
                        type: 'thickness'
                    },
                    {
                        label: 'ESAL评价标准表',
                        type: 'esal'
                    },
                ]
            },
            {
                label: '预测模型参数',
                type: 'model',
                sub: [
                    {
                        label: 'PCI预测模型参数',
                        type: 'pci'
                    },
                    {
                        label: 'RQI预测模型参数',
                        type: 'rqi'
                    }
                ]
            },
            {
                label: '养护决策参数',
                type: 'decision',
                sub: [
                    {
                        label: '决策树_沥青路面',
                        type: 'asphalt'
                    },
                    // {
                    //     label: '决策树_水泥路面',
                    //     type: 'cement'
                    // },
                    {
                        label: '养护对策信息表',
                        type: 'info'
                    },
                    {
                        label: '最低维修标准表',
                        type: 'fix'
                    }
                ]
            },
            {
                label: '公路技术状况评定等级',
                type: 'road',
            }
        ];
        return sidebars;
    })())
    .directive("sidebar", ['systemIF', 'Notify', '$route', 'sidebars', function(systemIF, Notify, $route, sidebars) {
        return {
            restrict: "E",
            link: function( $scope, element, attrs ) {
                $scope.sidebars = sidebars;
                $scope.mTabItemActive = 0;
                $scope.moduleName = $route.current.moduleName;
                $scope.tableName = $route.current.tableName;
                
                var getActiveItem = sidebars.filter(function(v, k) {
                    v.index = k;
                    return v.type == $route.current.moduleName;
                });

                if (getActiveItem.length) {
                    $scope.mTabItemActive = getActiveItem[0].index;
                }

                $scope.updateMtabItemActive = function (index) {
                    if ($scope.mTabItemActive === index) {
                        $scope.mTabItemActive = 0;
                        return;
                    }
                    $scope.mTabItemActive = index;
                };
            },
            templateUrl: sidebar
        }
    }])
    .service('systemIF', ['$http', 'commonData', 'Tool', function($http, commonData, Tool) {
        var systemIF = {
            listHdData: function (id, tableName) {
                return $http.get('/database/evaluation/'+ id +'/desc?tableName=' + tableName + '&signature=' + window.getSignature({
                    tableName: tableName
                }));
            },
            listData: function (id, params) {
                return $http.get('/database/evaluation/'+ id +'/getData', {
                    params: params
                });
            },
            update: function (id, params) {
                return $http.post('/database/evaluation/'+ id +'/update', params);
            },
            delete: function (id, params) {
                return $http.post('/database/evaluation/'+ id +'/delete', params);
            },
            enable: function (id, params) {
                return $http.post('/database/evaluation/'+ id +'/enable', params);
            },
            add: function (id, params) {
                return $http.get('/database/evaluation/'+ id +'/add', {
                    params: params
                });
            },
        };
        return angular.extend({}, commonData, systemIF);
    }])
;





