require('./modules/common/common.js');
require('./modules/index/index.js');
require('./modules/system/system.js');
require('./modules/data/data.js');
require('./modules/data/import.js');
require('./modules/data/delete.js');
require('./modules/analysis/analysis.js');
require('./modules/report/report.js');
require('./modules/user/user.js');
require('./modules/pwd/pwd.js');
require('./modules/log/log.js');
angular.module("app.modules", [
    'app.common',
    'app.user',
    'app.index',
    'app.data',
    'app.import',
    'app.delete',
    'app.system',
    'app.analysis',
    'app.report',
    'app.user',
    'app.pwd',
    'app.log'
]);