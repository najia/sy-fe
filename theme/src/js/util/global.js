/**
 * Created by Jessie on 2017/4/29.
 */
var CryptoJS = require("crypto-js");

function onlyNumber(obj) {
    // 得到第一个字符是否为负号
    // var t = obj.value.charAt(0);
    // 先把非数字的都替换掉，除了数字和.
    obj.value = obj.value.replace(/[^\d\.]/g, '');
    // 必须保证第一个为数字而不是.
    obj.value = obj.value.replace(/^\./g, '');
    // 保证只有出现一个.而没有多个.
    obj.value = obj.value.replace(/\.{2,}/g, '.');
    // 保证.只出现一次，而不能出现两次以上
    obj.value = obj.value.replace('.', '$#$').replace(/\./g, '').replace('$#$', '.');
    // 如果第一位是负号，则允许添加
    //if (t == '-') {
    //    obj.value = '-' + obj.value;
    //}
}

function onlyNumber2(obj) {
    if (obj.value.length == 1) {
        obj.value = obj.value.replace(/[^1-9]/g, '')
    } else {
        obj.value = obj.value.replace(/\D/g, '')
    }
}

function isContained(a, b) {
    if (!(a instanceof Array) || !(b instanceof Array)) return false;
    if (a.length < b.length) return false;
    for (var i = 0, len = b.length; i < len; i++) {
        if (a.indexOf(b[i].toString()) == -1) return false;
    }
    return true;
}

function printMap() {
    $('div.print-area').printArea({
        mode: 'popup',
        popClose: true
    });
}

function getAesString(data, key, iv) { //加密
    var key = CryptoJS.enc.Utf8.parse(key);
    var iv = CryptoJS.enc.Utf8.parse(iv);
    var encrypted = CryptoJS.AES.encrypt(data, key, {
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });
    return encrypted.toString(); //返回的是base64格式的密文
}

function getDAesString(encrypted, key, iv) { //解密
    var key = CryptoJS.enc.Utf8.parse(key);
    var iv = CryptoJS.enc.Utf8.parse(iv);
    var decrypted = CryptoJS.AES.decrypt(encrypted, key, {
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });
    return decrypted.toString(CryptoJS.enc.Utf8);
}

function getAES(data) { //加密
    var key = window.T.key; //密钥
    var iv = window.T.iv;
    var encrypted = getAesString(data, key, iv); //密文
    var encrypted1 = CryptoJS.enc.Utf8.parse(encrypted);
    return encrypted;
}

function getDAes(data) { //解密
    var key = window.T.key; //密钥
    var iv = window.T.iv;
    var decryptedStr = getDAesString(data, key, iv);
    return decryptedStr;
}

function getSignature(data) {
    var strs = [];
    var data = angular.copy(data);
    delete data.signature;
    // console.log(data);
    for (var i in data) {
        var key = i;
        // 判断是否是数组
        if (data[i] instanceof Array) {
            // 数组不为空转换为字符串， 否则不进入计算的字符里面
            if (data[i].length > 0) {
                data[i] = JSON.stringify(data[i]);
            } else {
                key = null;
            }            
            if (i == 'staticFields[]') {
                key = 'staticFields';
            }
            if (i == 'dynamicFields[]') {
                key = 'dynamicFields';
            }
        }
        if (key) {
            strs.push(key + '=' + data[i]);
        }
    }
    strs.sort(); // 数组排序
    strs = strs.join('&'); // 数组变字符串
    // console.log(strs);
    return CryptoJS.MD5(strs + window.T.key).toString();
}

window.onlyNumber = onlyNumber;
window.onlyNumber2 = onlyNumber2;
window.isContained = isContained;
window.printMap = printMap;
window.getAES = getAES;
window.getDAes = getDAes;
window.getSignature = getSignature;