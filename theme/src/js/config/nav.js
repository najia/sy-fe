
/** 
 * 往后退一个下标
 * 0-1
 * 1-0
*/
angular
    .module('app.config')
    .value('nav', [
        {
            label: '地理信息',
            type: 'index',
            status: 'index',
            className: 'glyphicon-home'
        },
        {
            label: '数据管理',
            type: 'data',
            status: 'data',
            className: 'glyphicon-log-in',
            sub: [
                {
                    label: '数据导入',
                    status: 'import',
                    type: 'import'
                },
                {
                    label: '数据删除',
                    status: 'delete',
                    type: 'delete'
                },
                {
                    label: '数据查询',
                    status: 'search',
                    type: 'search'
                }
            ]
        },
        {
            label: '参数设置',
            type: 'std',
            status: 'std',
            className: 'glyphicon-wrench'
        },
        {
            label: '数据查询',
            type: 'dsearch',
            status: 'dsearch',
            hide: true,
            className: 'glyphicon-log-in'
        },
        {
            label: '分析评价',
            type: 'analysis',
            status: 'analysis',
            className: 'glyphicon-comment'
        },
        {
            label: '统计报表',
            type: 'statistics',
            status: 'statistics',
            className: 'glyphicon-stats',
            sub: [
                {
                    label: '总报表',
                    status: 'report',
                    type: 'report'
                },
                {
                    label: '分区报表',
                    status: 'district',
                    type: 'district',
                    sub: []
                }
            ]
        },
        {
            label: '用户管理',
            type: 'user',
            status: 'user',
            className: 'glyphicon-user'
        },
        {
            label: '日志管理',
            type: 'log',
            status: 'log',
            className: 'glyphicon-edit'
        },
        {
            label: '修改密码',
            type: 'pwd',
            status: 'pwd',
            className: 'glyphicon-lock'
        }
    ])
;
