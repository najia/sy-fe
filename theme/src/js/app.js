require('../css/main.css');

require('./util/global.js');

// 内部模块
require('./config/global.config.js');
require('./config/nav.js');
require('./modules.js');
require('./login.js');
require('./util/http.js');
require('./modules/common/common.js');

angular
  .module('app', [
    'ngRoute',
    'ui.bootstrap',
    'angularFileUpload',
    'http-3rd',
    'ui.select',
    'app.common',
    'app.config',
    'app.modules'
  ])
  .config(['$routeProvider', 'uibPaginationConfig', '$localeProvider', 'uibDatepickerConfig', 'uibDatepickerPopupConfig', function ($routeProvider, uibPaginationConfig, $localeProvider, uibDatepickerConfig, uibDatepickerPopupConfig) {
    $routeProvider.otherwise({
      redirectTo: '/index'
      // 4 日志管理  修改密码
      // 3 数据查询 统计报表 修改密码
      // 2 数据查询 统计报表 修改密码
      // 1 用户管理 修改密码
    });
    angular.extend(uibPaginationConfig, {
      'nextText': '›',
      'firstText': '«',
      'lastText': '»',
      'previousText': '‹',
      'maxSize': 5
    });
    angular.extend(uibDatepickerConfig, {
      formatDayTitle: 'yyyy-MM',
      showButtonBar: false
    });
    angular.extend(uibDatepickerPopupConfig, {
      'currentText': '今天',
      'clearText': '清除',
      'closeText': '关闭',
      'showButtonBar': true
    });
    //将日期的周几的英文表示改成中文表示
    //在angularjs文件的 $LocaleProvider 里可以查看具体情况
    var clone = $localeProvider.$get();
    $localeProvider.$get = function () {
      var $locale = {};
      angular.merge($locale, clone, {
        DATETIME_FORMATS: {
          SHORTDAY: '日,一,二,三,四,五,六'.split(','),
          MONTH: '一月,二月,三月,四月,五月,六月,七月,八月,九月,十月,十一月,十二月'
            .split(',')
        }
      });
      return $locale;
    };
  }])
  .run(['$rootScope', '$route', 'global', 'nav', 'Notify', 'Alert', '$routeParams', '$http', '$location', function ($rootScope, $route, global, nav, Notify, Alert, $routeParams, $http, $location) {
    $rootScope.navs = nav;
    $rootScope.global = global;
    $rootScope.Notify = Notify;
    $rootScope.Alert = Alert;
    $rootScope.yearList = [];
    $rootScope.globalYear = '';
    $rootScope.uploading = false;
    $rootScope.stopUploading = false; // 是否是中断状态

    $rootScope.showPageNoDataTip = false; // 是否显示没数据提示
    $rootScope.globalCanEdit = true;

    $rootScope.activeIndex = -1;
    $rootScope.activeSonIndex = -1;
    $rootScope.activeSmallIndex = -1;

    $rootScope.globalWatch = {};

    $rootScope.globalType = 0;
    $rootScope.dataType = [{
      id: 0,
      label: '年份数据'
    }, {
      id: 1,
      label: '自定义数据'
    }];
    var isGoHome = false; // 判断是否跳到首页，如果要跳到首页，需要清空 watch
    $rootScope.globalDistrictList = [];

    // 根据权限设置菜单
    function setMenu() {
      // 区属管理员菜单
      if ($rootScope.globalUserinfo.currentUser.user_type == 3) {
        // downtonw suburbs 只是为了区分选中的是市区的菜单，还是郊区的菜单，区属管理员可随便定义
        $rootScope.districtType = 'downtown';
        $rootScope.navs[2].hide = true;
        $rootScope.navs[1].hide = true;
        $rootScope.navs[4].hide = true;
        $rootScope.navs[5].sub = null;
        $rootScope.navs[6].hide = true;
        $rootScope.navs[7].hide = true;
        $rootScope.navs[3].hide = false;

      }

      // 市区管理员
      if ($rootScope.globalUserinfo.currentUser.user_type == 2) {
        $rootScope.navs[6].hide = true;
        $rootScope.navs[7].hide = true;

        // 【参数设置】【数据管理】【分析评价】
        $rootScope.navs[1].hide = !$rootScope.globalCanEdit;
        $rootScope.navs[2].hide = !$rootScope.globalCanEdit;
        $rootScope.navs[3].hide = $rootScope.globalCanEdit;
        $rootScope.navs[4].hide = !$rootScope.globalCanEdit;       
      }

      // 超级管理员
      if ($rootScope.globalUserinfo.currentUser.user_type == 1) {
        $rootScope.navs[2].hide = true;
        $rootScope.navs[1].hide = true;
        $rootScope.navs[4].hide = true;
        $rootScope.navs[5].hide = true;
        $rootScope.navs[7].hide = true;
      }

      // 审计管理员
      if ($rootScope.globalUserinfo.currentUser.user_type == 4) {
        $rootScope.navs[2].hide = true;
        $rootScope.navs[1].hide = true;
        $rootScope.navs[4].hide = true;
        $rootScope.navs[5].hide = true;
        $rootScope.navs[6].hide = true;
      }
    }
    // 统计报表分区数据
    function setDistrictMenu() {
      if ($rootScope.globalUserinfo.currentUser.user_type != 2) {
        return;
      }
      // 检测数据
      var newSidebarData = [];
      $http.get('/report/district/summary/getDistrictList', {
        params: {
          year: $rootScope.globalYear,
          noTip: true,
        }
      }).success(function (data) {
        if (data.status == 0 && data.data) {
          var getData = data.data;
          angular.forEach(getData, function (v, k) {
            angular.forEach(v.list, function (value, key) {
              var objSub = {
                label: value.district,
                status: 'partition',
              }
              newSidebarData.push(objSub);
            });
          });
          // 如果是地区数据更换，需要赋值后跳转到总报表那边。
          $rootScope.navs[4].sub[1].sub = newSidebarData;
          $rootScope.globalDistrictList = newSidebarData; 
        } else {
          $rootScope.navs[4].sub[1].sub = [];
        }
      });
    }

    // 全局取消 watch
    function resetWatch() {
      console.log(isGoHome, 'isGoHome')
      /**
       * isGoHome 用于用户没权限进入当前页面引起的跳转，需要去掉所有记录的监听
       * 每个页面的 destroy 方法是为了去掉由于用户自己触发跳转需要去掉当前页面的监听
       */
      var globalWatch = $rootScope.globalWatch;
      for (var key in globalWatch) {
        // 为了处理菜单只显示一级带来的问题，必须清理 watch
        if (isGoHome || $route.current.state == 'dsearch' || $route.current.state == 'search') {
          globalWatch[key] && globalWatch[key]();
        }
      }
      isGoHome = false;
    }

    // 全局路由控制， 防止没权限的通过浏览器输入栏直接进入页面
    function setUrl(state) {
      // 判断区属管理员是否有权限进入当前菜单
      if ($rootScope.globalUserinfo.currentUser.user_type == 3) {
        // 可进去的菜单如下
        var canEntryMenu = ['index', 'dsearch', 'statistics', 'district', 'pwd'];
        //  如果不可编辑，要把 search 从可进入的数组里面去掉
        if (canEntryMenu.indexOf(state) == -1) {
          isGoHome = true;
          $location.path('/index');
        }
      }

      // 判断超级管理员是否有权限进入当前菜单
      if ($rootScope.globalUserinfo.currentUser.user_type == 1) {
        // 可进去的菜单如下
        var canEntryMenu = ['index', 'user', 'pwd'];
        if (canEntryMenu.indexOf(state) == -1) {
          $location.path('/index');
        }
      }

      // 判断市区管理员是否有权限进入当前菜单
      if ($rootScope.globalUserinfo.currentUser.user_type == 2) {
        // 不可进去的菜单如下
        var canNotEntryMenu = ['user', 'log'];
        if (!$rootScope.globalCanEdit) {
          canNotEntryMenu.push('std', 'import', 'delete', 'analysis');
        }
        if (canNotEntryMenu.indexOf(state) >= 0) {
          isGoHome = true;
          $location.path('/index');
        }
      }

      // 判断审计管理员是否有权限进入当前菜单
      if ($rootScope.globalUserinfo.currentUser.user_type == 4) {
        // 可进去的菜单如下
        var canEntryMenu = ['index', 'log', 'pwd'];
        if (canEntryMenu.indexOf(state) == -1) {
          $location.path('/index');
        }
      }
    }

    // 数据库下拉框更改状态
    $rootScope.globalDataChange = function () {
      if (window.T) {
        localStorage.setItem('globalYear', $rootScope.globalYear);
        if (window.T.expire[$rootScope.globalYear]) {
            $rootScope.globalCanEdit = window.T.expire[$rootScope.globalYear].can_edit;
        }
        if (window.T.container[$rootScope.globalYear]) {
          $rootScope.containerHasData = window.T.container[$rootScope.globalYear].has_data;
        } else {
          $rootScope.containerHasData = true;
        }
        setMenu();
        setDistrictMenu();
        setUrl($route.current.state);
        resetWatch();

        // 不可编辑状态下面，并且是在 search 页面，需要调转到 dsearch， 为了菜单只显示一级。
        if (!$rootScope.globalCanEdit && $route.current.state == 'search') {
          $location.path('/dsearch');
        } else if ($rootScope.globalCanEdit && $route.current.state == 'dsearch') {
          $location.path('/data/search');
        }
      }
    }

    // 主页面逻辑
    if (window.T && window.T.currentURI == 'syweb/welcome') {
      var globalYear = localStorage.getItem('globalYear');
      $rootScope.yearList = window.T.years;
      // 如果缓存里面有值，就从缓存里面拿
      if (globalYear) {
        $rootScope.globalYear = globalYear;
      } else {
        $rootScope.globalYear = $rootScope.yearList[$rootScope.yearList.length - 1] || '';
      }
      if (window.T.expire[$rootScope.globalYear]) {
        $rootScope.globalCanEdit = window.T.expire[$rootScope.globalYear].can_edit;
    }
      $rootScope.globalUserinfo = window.T;
      $rootScope.containerHasData = true;
      if (window.T.container[$rootScope.globalYear]) {
        $rootScope.containerHasData = window.T.container[$rootScope.globalYear].has_data;
      }
      if ($rootScope.globalUserinfo.currentUser.user_type == 1) {
        $rootScope.globalUsername = '辽宁省路政局总管理员';
      } else if ($rootScope.globalUserinfo.currentUser.user_type == 3) {
        $rootScope.globalUsername = $rootScope.globalUserinfo.currentUser.district + '区管理员';
      } else if ($rootScope.globalUserinfo.currentUser.user_type == 2) {
        $rootScope.globalUsername = '业务系统总管理员';
      } else if ($rootScope.globalUserinfo.currentUser.user_type == 4) {
        $rootScope.globalUsername = '审计';
      }

      setMenu();
      setDistrictMenu();
    }

    // 路由跳转之前
    $rootScope.$on('$routeChangeStart', function (ev, to) {
      // 如果用户未登录
      // if (!AuthService.userLoggedIn()) {
      //     if (next.templateUrl === "login.html") {
      //         // 已经转向登录路由因此无需重定向
      //     } else {
      //         $location.path('/login');
      //     }
      // }
      if (window.T && window.T.currentURI == 'syweb/welcome') {
        if (to.$$route) {
          setUrl(to.$$route.state);
        }
      }
    });

    $rootScope
      .$on('$routeChangeSuccess', function () {
        $rootScope.navDistrict = $routeParams.district;
        var siteState = $rootScope.siteState = $route.current.state;
        nav.forEach(function(v, k) {
          if (v.status == siteState) {
            $rootScope.activeIndex = k;
          }
          if (v.sub) {
            v.sub.forEach(function(value, key) {
              if (value.status == siteState) {
                $rootScope.activeIndex = k;
                $rootScope.activeSonIndex = key;
              }
            });
          }
        });
        $rootScope.username = angular.element('.user-links a:first').text();
      })
      ;

    $rootScope.toggleSub = function (index, item) {
      if (item && item.readonly) {
        return;
      }
      $rootScope.activeSonIndex = -1;
      if (index == $rootScope.activeIndex) {
        $rootScope.activeIndex = -1;
      } else {
        $rootScope.activeIndex = index;
      }
    }
    $rootScope.toggleSon = function (index) {
      if (index == $rootScope.activeSonIndex) {
        $rootScope.activeSonIndex = -1;
      } else {
        $rootScope.activeSonIndex = index;
      }
    }

    $rootScope.toggleSmall = function (index) {
      if (index == $rootScope.activeSmallIndex) {
        $rootScope.activeSmallIndex = -1;
      } else {
        $rootScope.activeSmallIndex = index;
      }
    }
  }])
  ;

angular.element(document).ready(function () {
  angular.bootstrap(document, ["app"]);
});