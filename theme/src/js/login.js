/**
 * Created by Jessie on 2017/4/25.
 */

function init() {
    var loginForm = $('#form-sign');
    if (!loginForm.length) return;
    loginForm.validate({
        rules: {
            'username': {required: true},
            'password': {required: true},
            'captcha': {required: true}
        },
        errorPlacement: function(error, element){
            error.appendTo(element.parent());
        },
        messages: {
            'username': {
                required: '请输入用户名'
            },
            'password': {
                required: '请输入密码'
            },
            'captcha': {
                required: '请输入验证码'
            }
        },
        submitHandler:function(form)
        {
            var test = window.getAES('pms_admin');
            console.log(window.getDAes(test));
            $('#login-alert').hide();
            var obj = {
                username: window.getAES($('#username').val()),
                password: window.getAES($('#password').val()),
                captcha: $('#captcha').val()
            }
            $.post('/admin/login', obj, function (data) {
                if (data.status == 0) {
                    $('#login-alert').hide();
                    window.location.href = '/gateway';
                } else {
                    $('#login-alert').show().text(data.msg);
                    $('.form-sign img').prop('src', '/admin/captcha?' + new Date().getTime());
                }
            }, 'json');
        }

    });
}

function handleCode() {
    var $this = $(this);
    $this.prop('src', '/admin/captcha?' + new Date().getTime());
}


init();

$('.form-sign')
    .on('click', 'img', handleCode);